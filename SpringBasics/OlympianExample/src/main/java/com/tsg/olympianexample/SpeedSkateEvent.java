/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.olympianexample;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class SpeedSkateEvent implements Event {

    @Override
    public String compete() {
        System.out.println("Skating REALLY fast!!!!");
        Random rnd = new Random();
        
        try {
            Thread.sleep(1000 + rnd.nextInt(1000));
        } catch (InterruptedException ex) {
            Logger.getLogger(SpeedSkateEvent.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("And Done!");
        return "SpeedSkate";
    }
    
}
