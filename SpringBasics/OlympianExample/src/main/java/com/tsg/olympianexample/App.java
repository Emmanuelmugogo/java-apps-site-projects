/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.olympianexample;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class App {

    public static void main(String[] args) {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SkiJumper sj = (SkiJumper) ctx.getBean("superSkiJumper");

        sj.competeInEvent();

        Event skiJumpEvent = new SkiJumpEvent();
        Olympian olympianSkiJumper = new Olympian(skiJumpEvent);
        olympianSkiJumper.competeInEvent();

//        Olympian usaSkiJumper = (Olympian) ctx.getBean("usaSkiJumper"); //Same as line 29.
        Olympian usaSkiJumper = ctx.getBean("usaSkiJumper", Olympian.class);
        usaSkiJumper.competeInEvent();
        
        Olympian usaSpeedSkater = ctx.getBean("usaSpeedSkater", Olympian.class);
        usaSpeedSkater.competeInEvent();
        
        Olympian canadianSpeedSkater = ctx.getBean("canadaSpeedSkater", Olympian.class);
        canadianSpeedSkater.competeInEvent();
        
        System.out.println("-------------------------WARNING: EXPERIMENTS BELOW!!! -------------------");
        Event sampleEvent = (Event) ctx.getBean("skiJumping");
        sampleEvent.compete();
        

    }

}
