/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.addressbook.dao;

import com.tsg.addressbook.dto.Address;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class AddressListDaoInMemImpl implements AddressListDao {
    
    private Map<Integer, Address> addressMap = new HashMap<>();
    private static int addressIdCounter = 0;

    @Override
    public Address addAddress(Address address) {
        address.setAddressId(addressIdCounter);
        addressIdCounter++;
        addressMap.put(address.getAddressId(), address);
        return address;
    }

    @Override
    public void removeAddress(int addressId) {
        addressMap.remove(addressId);
    }

    @Override
    public void updateAddress(Address address) {
        addressMap.put(address.getAddressId(), address);
    }

    @Override
    public List<Address> getAllAddresses() {
        Collection<Address> a = addressMap.values();
        return new ArrayList(a);
    }

    @Override
    public Address getAddressById(int addressId) {
        return addressMap.get(addressId);
    }

    @Override
    public List<Address> searchAddresses(Map<SearchTerm, String> criteria) {
        String firstNameCriteria = criteria.get(SearchTerm.FIRST_NAME);
        String lastNameCriteria = criteria.get(SearchTerm.LAST_NAME);
        String streetAddressCriteria = criteria.get(SearchTerm.STREET_ADDRESS);
        String cityCriteria = criteria.get(SearchTerm.CITY);
        String stateCriteria = criteria.get(SearchTerm.STATE);
        String zipCodeCriteria = criteria.get(SearchTerm.ZIP_CODE);
        
        Predicate<Address> firstNameMatches;
        Predicate<Address> lastNameMatches;
        Predicate<Address> streetAddressMatches;
        Predicate<Address> cityMatches;
        Predicate<Address> stateMatches;
        Predicate<Address> zipCodeMatches;
        
        Predicate<Address> truePredicate = (a) -> {return true;};
        firstNameMatches = (firstNameCriteria == null || firstNameCriteria.isEmpty())? truePredicate: (a)-> a.getFirstName().equals(firstNameCriteria);
        lastNameMatches = (lastNameCriteria == null || lastNameCriteria.isEmpty())? truePredicate: (a)-> a.getLastName().equals(lastNameCriteria);
        streetAddressMatches = (streetAddressCriteria == null || streetAddressCriteria.isEmpty())? truePredicate: (a)-> a.getStreetAddress().equals(streetAddressCriteria);
        cityMatches = (cityCriteria == null || cityCriteria.isEmpty())? truePredicate: (a)-> a.getCity().equals(cityCriteria);
        stateMatches = (stateCriteria == null || stateCriteria.isEmpty())? truePredicate: (a)-> a.getState().equals(stateCriteria);
        zipCodeMatches = (zipCodeCriteria == null || zipCodeCriteria.isEmpty())? truePredicate: (a)-> a.getZipCode().equals(zipCodeCriteria);
        
        return addressMap.values().stream()
                .filter(firstNameMatches
                        .and(lastNameMatches)
                        .and(streetAddressMatches)
                        .and(cityMatches)
                        .and(stateMatches)
                        .and(zipCodeMatches))
                .collect(Collectors.toList());
        
    }
    
}
