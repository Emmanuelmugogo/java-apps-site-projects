/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.addressbook;

import com.tsg.addressbook.dao.AddressListDao;
import com.tsg.addressbook.dto.Address;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */

@Controller
public class HomeControllerNoAjax {
    private final AddressListDao dao;
    
    @Inject
    public HomeControllerNoAjax(AddressListDao dao)
    {
        this.dao = dao;
    }

@RequestMapping(value="/displayAddressListNoAjax", method=RequestMethod.GET)
    public String displayAddressListNoAjax(Model model)
    {
        List<Address> aList = dao.getAllAddresses();
        model.addAttribute("addressList",aList);
        
        return "displayAddressListNoAjax";
    }
    
    @RequestMapping(value="displayNewAddressFormNoAjax", method=RequestMethod.GET)
    public String displayNewAddressFormNoAjax(Model model){
        Address address = new Address();
        model.addAttribute(address);
        return "newAddressFormNoAjax";
    }
    
    @RequestMapping(value="/addNewAddressNoAjax", method=RequestMethod.POST)
    public String addNewAddressNoAjax(@Valid @ModelAttribute("address") Address address, BindingResult result, HttpServletRequest req){
        
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String streetAddress = req.getParameter("streetAddress");
        String city = req.getParameter("city");
        String state = req.getParameter("state");
        String zipCode = req.getParameter("zipCode");
        
//        Address address = new Address();
        address.setFirstName(firstName);
        address.setLastName(lastName);
        address.setStreetAddress(streetAddress);
        address.setCity(city);
        address.setState(state);
        address.setZipCode(zipCode);
       
        
        if(result.hasErrors()){
            return "newAddressFormNoAjax";
        }
        dao.addAddress(address);
        
        return "redirect:displayAddressListNoAjax";
    }
    
    @RequestMapping(value="/deleteAddressNoAjax", method=RequestMethod.GET)
    public String deleteAddressNoAjax(HttpServletRequest req){
        
        int addressId = Integer.parseInt(req.getParameter("addressId"));
        
        dao.removeAddress(addressId);
        
        return "redirect:displayAddressListNoAjax";
    }
    
    @RequestMapping(value="/displayEditAddressFormNoAjax", method=RequestMethod.GET)
    public String displayEditAddressFormNoAjax(HttpServletRequest req, Model model){
        int addressId = Integer.parseInt(req.getParameter("addressId"));
        
        Address address = dao.getAddressById(addressId);
        
        model.addAttribute("address", address);
        
        return "editAddressFormNoAjax";
    }
    
    @RequestMapping(value="/editAddressNoAjax", method=RequestMethod.POST)
    public String editAddressNoAjax(@Valid @ModelAttribute("address") Address address, BindingResult result){
        
        if(result.hasErrors()){
            return "editAddressFormNoAjax";
        }
        dao.updateAddress(address);
        
        return "redirect:displayAddressListNoAjax";
    }
}
