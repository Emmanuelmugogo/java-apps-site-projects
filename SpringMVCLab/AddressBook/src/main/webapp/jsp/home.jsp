<%-- 
    Document   : home
    Created on : Mar 28, 2016, 4:07:27 PM
    Author     : apprentice
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Address Book</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel='shortcut icon' href="${pageContext.request.contextPath}/img/icon.png">
        <style>
            .backGroundPic {
                background-image: url(img/107.jpg);
                background-size: 100%;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="jumbotron" style="background-image: url(img/address.jpg); background-size: 100%;">
                <!--<div class="jumbotron">-->
                <h1>ADDRESS BOOK</h1>
                <p>Welcome to the Address Book with MVC and validation.</p>
                <!--<p><a class="btn btn-primary btn-lg" href="${pageContext.request.contextPath}/home" role="button">Learn more</a></p>-->
            </div>
        </div>
        <div class="container">
            <!--<h1>Address Book</h1>-->
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role='presentation' class="active">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role='presentation' >
                        <a href="${pageContext.request.contextPath}/rest">Rest</a>
                    </li>
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/search">Search</a>
                    </li>
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/stats">Stats</a>
                    </li>
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/displayAddressListNoAjax">
                            Address List (No Ajax)
                        </a>
                    </li>
                </ul>
            </div>          


            <div class="row">
                <div class="col-md-6" id="507picture">
                    <h2>My Addresses</h2>
                    <%@include file="addressSummaryTableFragment.jsp"%> 
                </div>
                <!--start of add form-->
                <div class="col-md-6">
                    <h2>Add New Address</h2>
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="add-first-name" class="col-md-4 control-label">First Name:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-first-name" placeholder="First Name" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-last-name" class="col-md-4 control-label">Last Name:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-last-name" placeholder="Last Name" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-street-address" class="col-md-4 control-label">Street Address:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-street-address" placeholder="Street Address" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-city" class="col-md-4 control-label">City:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-city" placeholder="City" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-state" class="col-md-4 control-label">State:</label>
                            <div class="col-md-8">
                                <input type="tel" class="form-control" id="add-state" placeholder="State" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-zip-code" class="col-md-4 control-label">Zip Code:</label>
                            <div class="col-md-8">
                                <input type="tel" class="form-control" id="add-zip-code" placeholder="Zip Code" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <button type="submit"
                                        id="add-button"
                                        class="btn btn-default">Create Address</button>
                            </div>
                        </div>
                    </form>
                    <div id="validationErrors" style="color: red" />
                </div>
            </div>
        </div>
    </div>

    <%@include file="detailsEditModalFragment.jsp"%>


    <script src='${pageContext.request.contextPath}/js/jquery-1.11.3.min.js'></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/mockData.js"></script>
    <script src="${pageContext.request.contextPath}/js/addressList.js"></script>


</body>
</html>
