<!DOCTYPE html>
<html>
    <head>
        <title>ADDRESS BOOK</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css"
              rel="stylesheet">
        <link rel='shortcut icon' href="${pageContext.request.contextPath}/img/icon.png">
        <style>
            .backGroundPic {
                background-image: url(img/107.jpg);
                background-size: 100%;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="jumbotron" style="background-image: url(img/address.jpg); background-size: 100%;">
                <h1>ADDRESS BOOK</h1>
                <p>Welcome to the Address Book with MVC and validation.</p>
                <!--<p><a class="btn btn-primary btn-lg" href="${pageContext.request.contextPath}/home" role="button">Learn more</a></p>-->
            </div>
        </div>
        <div class="container">
            <!--<h1>Address Book</h1>-->
            <!--<hr/>-->
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role='presentation' >
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role='presentation' class="active">
                        <a href="${pageContext.request.contextPath}/rest">Rest</a>
                    </li>
                    <li role='presentation' >
                        <a href="${pageContext.request.contextPath}/search">Search</a>
                    </li>
                    <li role='presentation' >
                        <a href="${pageContext.request.contextPath}/stats">Stats</a>
                    </li>
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/displayAddressListNoAjax">
                            Address List (No Ajax)
                        </a>
                    </li>
                </ul>
            </div>          

            <!--        <div class="container">
                        <h1>Address Application</h1>
                        <hr/>-->
            <!--<div class="navbar">-->
            <!--<ul class="nav nav-tabs">-->
            <!--<li role="presentation" class="active">-->
            <!--<a href="home">Home</a>-->
            <!--</li>-->
            <!--<li role="presentation">-->
            <!--<a href="search">Search</a>-->
            <!--</li>-->
            <!--<li role="presentation">-->
            <!--<a href="stats">Stats</a>-->
            <!--</li>-->
            <!--</ul>-->
            <!--</div>-->
            <!--
            Add a row to our container - this will hold the summary table and the new contact form.
            -->
            <div class="row">
                <!-- #2: Add a col to hold the summary table - have it take up half the row -->
                <div class="col-md-6">
                    <div id="addressTableDiv">
                        <h2>My Addresses</h2>

                        <table id="addressTable" class="table table-hover">
                            <tr>
                                <th width="40%">Address Name</th>
                                <th width="30%">City</th>
                                <th width="15%"></th>
                                <th width="15%"></th>
                            </tr>
                            <!--
                             #3: This holds the list of contacts - we will add rows
                            dynamically
                             using jQuery
                            -->
                            <tbody id="contentRows"></tbody>
                        </table>
                    </div>
                </div> <!-- End col div -->
                <!--
                #4: Add col to hold the new contact form - have it take up the other half of the row
                -->
                <div class="col-md-6">

                    <div id="editFormDiv" style="display: none">
                        <h2 onclick="hideEditForm()">Edit Address</h2>

                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="add-first-name" class="col-md-4 control-label">
                                    First Name:
                                </label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-first-name"
                                           placeholder="First Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-last-name" class="col-md-4 control-label">
                                    Last Name:
                                </label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-last-name"
                                           placeholder="Last Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-street-address" class="col-md-4 control-label">
                                    Street Address:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-street-address"
                                           placeholder="Street Address"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-city" class="col-md-4 control-label">City:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-city"
                                           placeholder="City"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-state" class="col-md-4 control-label">State:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-state"
                                           placeholder="State"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-zip-code" class="col-md-4 control-label">Zip Code:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-zip-code"
                                           placeholder="Zip Code"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-4">
                                    <input type="hidden" id="edit-address-id">
                                    <button type="button"
                                            id="edit-cancel-button"
                                            class="btn btn-default"
                                            onclick="hideEditForm()">
                                        Cancel
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button type="submit"
                                            id="edit-button"
                                            class="btn btn-default">
                                        Update Address
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div id="addFormDiv">

                        <h2>Add New Address</h2>

                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="add-first-name" class="col-md-4 control-label">
                                    First Name:
                                </label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-first-name"
                                           placeholder="First Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-last-name" class="col-md-4 control-label">
                                    Last Name:
                                </label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-last-name"
                                           placeholder="Last Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-street-address" class="col-md-4 control-label">
                                    Street Address:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-street-address"
                                           placeholder="Street Address"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-city" class="col-md-4 control-label">City:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-city"
                                           placeholder="City"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-state" class="col-md-4 control-label">State:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-state"
                                           placeholder="State"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-zip-code" class="col-md-4 control-label">Zip Code:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-zip-code"
                                           placeholder="Zip Code"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <button type="submit"
                                            id="add-button"
                                            class="btn btn-default">
                                        Create Address
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> <!-- End col div -->
                <div id="validationErrors" style="color: red" />
            </div> <!-- End row div -->
        </div>
    </div>
    <!-- #5: Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/restAddressList.js"></script>
</body>
</html>
