<%-- 
    Document   : newAddressFormNoAjax
    Created on : Mar 29, 2016, 4:18:09 PM
    Author     : apprentice
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Address Book</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel='shortcut icon' href="${pageContext.request.contextPath}/img/icon.png">
        <style>
            .backGroundPic {
                background-image: url(img/107.jpg);
                background-size: 100%;
            }
        </style>
    </head>
    <body>
        <div class='container'>
            <div class="container">
                <div class="jumbotron" style="background-image: url(img/address.jpg); background-size: 100%;">
                    <h1>ADDRESS BOOK</h1>
                    <p>Welcome to the Address Book with MVC and validation.</p>
                    <!--<p><a class="btn btn-primary btn-lg" href="${pageContext.request.contextPath}/home" role="button">Learn more</a></p>-->
                </div>
            </div> <!--<h1>Address Book</h1>-->
            <hr/>
        </div>
        <div class="container">
            <h1>New Address Form</h1>
            <a href="displayAddressListNoAjax">Address List (No Ajax)</a>
            <hr/>
            <sf:form class="form-horizontal" modelAttribute="address"
                     role="form"
                     action="addNewAddressNoAjax"
                     method="POST">
                <div class="form-group">
                    <label for="add-first-name" class="col-md-4 control-label">First Name:</label>
                    <div class='col-md-8'>
                        <sf:input type="text" class="form-control" id="add-first-name" path="firstName" placeholder="First Name"/>
                        <sf:errors path="firstName" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-last-name" class="col-md-4 control-label">Last Name:</label>
                        <div class='col-md-8'>
                        <sf:input type="text" class="form-control" id="add-last-name" path="lastName" placeholder="Last Name"/>
                        <sf:errors path="lastName" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-street-address" class="col-md-4 control-label">Street Address:</label>
                        <div class='col-md-8'>
                        <sf:input type="text" class="form-control" id="add-street-address" path="streetAddress" placeholder="Street Address"/>
                        <sf:errors path="streetAddress" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-city" class="col-md-4 control-label">City:</label>
                        <div class='col-md-8'>
                        <sf:input type="text" class="form-control" id="add-city" path="city" placeholder="City"/>
                        <sf:errors path="city" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-state" class="col-md-4 control-label">State:</label>
                        <div class='col-md-8'>
                        <sf:input type="text" class="form-control" id="add-state" path="state" placeholder="State"/>
                        <sf:errors path="state" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-zip-code" class="col-md-4 control-label">Zip Code:</label>
                        <div class='col-md-8'>
                        <sf:input type="text" class="form-control" id="add-zip-code" path="zipCode" placeholder="Zip Code"/>
                        <sf:errors path="zipCode" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8">
                            <button type="submit" id="edit-button" class="btn btn-default">Add New Address</button>
                        </div>    
                    </div>
            </sf:form>
        </div>
    </body>
</html>
