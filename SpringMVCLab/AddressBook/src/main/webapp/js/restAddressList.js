$(document).ready(function () {

    loadAddresses();

    $('#add-button').click(function (event) {

        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/AddressBook/address',
            data: JSON.stringify({
                firstName: $('#add-first-name').val(),
                lastName: $('#add-last-name').val(),
                streetAddress: $('#add-street-address').val(),
                city: $('#add-city').val(),
                state: $('#add-state').val(),
                zipCode: $('#add-zip-code').val()

            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            $('#add-first-name').val('');
            $('#add-last-name').val('');
            $('#add-street-address').val('');
            $('#add-city').val('');
            $('#add-state').val('');
            $('#add-zip-code').val('');
            loadAddresses();
            clearErrors();
        }).error(function (data, status) {
            clearErrors();
            $.each(data.responseJSON.fieldErrors, function (index, validationError) {
                var errorDiv = $('#validationErrors');
                errorDiv.append(validationError.message).append($('<br>'));
            });
        });
    });

    $('#edit-button').click(function (event) {
        event.preventDefault();

        $.ajax({
            type: 'PUT',
            url: 'address/' + $('#edit-address-id').val(),
            data: JSON.stringify({
                firstName: $('#edit-first-name').val(),
                lastName: $('#edit-last-name').val(),
                streetAddress: $('#edit-streetAddress').val(),
                city: $('#edit-city').val(),
                state: $('#edit-state').val(),
                zipCode: $('#edit-zip-code').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            hideEditForm();
            loadAddresses();
            clearErrors();
        }).error(function (data, status) {
            clearErrors();
            $.each(data.responseJSON.fieldErrors, function (index, validationError) {
                var errorDiv = $('#validationErrors');
                errorDiv.append(validationError.message).append($('<br>'));
            });
        });
    });

});

function loadAddresses() {
    clearAddressTable();

    var contentRows = $('#contentRows');

    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/AddressBook/addresses'
    }).success(function (data, status) {
        $.each(data, function (index, address) {
            var name = address.firstName + ' ' + address.lastName;
            var city = address.city;
            var id = address.addressId;

            var row = '<tr>';
            row += '<td>' + name + '</td>';
            row += '<td>' + city + '</td>';
            row += '<td><a onclick="showEditForm(' + id + ')">Edit</a></td>';
            row += '<td><a onclick="deleteAddress(' + id + ')">Delete</a></td>';
            row += '</tr>';
            contentRows.append(row);
        });
    });
}

function clearAddressTable() {
    $('#contentRows').empty();
}

function deleteAddress(addressId) {
    $.ajax({
        type: 'DELETE',
        url: 'address/' + addressId
    }).success(function () {
        hideEditForm();
        loadAddresses();
    });
}

function showEditForm(addressId) {
    $.ajax({
        type: 'GET',
        url: 'address/' + addressId
    }).success(function (address, status) {
        $('#edit-first-name').val(address.firstName);
        $('#edit-last-name').val(address.lastName);
        $('#edit-street-address').val(address.streetAddress);
        $('#edit-city').val(address.city);
        $('#edit-state').val(address.state);
        $('#edit-zip-code').val(address.zipCode);
        $('#edit-address-id').val(address.addressId);
        $('#editFormDiv').show();
        $('#addFormDiv').hide();
    });
}

function hideEditForm() {

    $('#edit-first-name').val('');
    $('#edit-last-name').val('');
    $('#edit-street-address').val('');
    $('#edit-city').val('');
    $('#edit-state').val('');
    $('#edit-zip-code').val('');
    $('#edit-address-id').val('');
    $('#addFormDiv').show();
    $('#editFormDiv').hide();
}

function clearErrors() {
    $('#validationErrors').empty();
}


