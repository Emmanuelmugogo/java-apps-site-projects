/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    loadAddresses();

    $('#add-button').click(function (event) {

        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'address',
            data: JSON.stringify({
                firstName: $('#add-first-name').val(),
                lastName: $('#add-last-name').val(),
                streetAddress: $('#add-street-address').val(),
                city: $('#add-city').val(),
                state: $('#add-state').val(),
                zipCode: $('#add-zip-code').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            $('#add-first-name').val('');
            $('#add-last-name').val('');
            $('#add-street-address').val('');
            $('#add-city').val('');
            $('#add-state').val('');
            $('#add-zip-code').val('');
            loadAddresses();
            clearErrors();
        }).error(function (data, status) {
            clearErrors();
            $.each(data.responseJSON.fieldErrors, function (index, validationError) {
                var errorDiv = $('#validationErrors');
                errorDiv.append(validationError.message).append($('<br>'));
            });
        });
    });

    $('#edit-button').click(function (event) {
        event.preventDefault();

        $.ajax({
            type: 'PUT',
            url: 'address/' + $('#edit-address-id').val(),
            data: JSON.stringify({
                firstName: $('#edit-first-name').val(),
                lastName: $('#edit-last-name').val(),
                streetAddress: $('#edit-street-address').val(),
                city: $('#edit-city').val(),
                state: $('#edit-state').val(),
                zipCode: $('#edit-zip-code').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
//            hideEditForm();
            loadAddresses();
        });
    });
});

// on click for our search button
$('#search-button').click(function (event) {
    // we don’t want the button to actually submit
    // we'll handle data submission via ajax
    event.preventDefault();
    $.ajax({
        type: 'POST',
        url: 'search/addresses',
        data: JSON.stringify({
            firstName: $('#search-first-name').val(),
            lastName: $('#search-last-name').val(),
            streetAddress: $('#search-street-address').val(),
            city: $('#search-city').val(),
            state: $('#search-state').val(),
            zipCode: $('#search-zip-code').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function (data, status) {
        $('#search-first-name').val('');
        $('#search-last-name').val('');
        $('#search-street-address').val('');
        $('#search-city').val('');
        $('#search-state').val('');
        $('#search-zip-code').val('');
        fillAddressTable(data, status);
    });
});

function loadAddresses() {
    // Make an Ajax GET call to the 'contacts' endpoint. Iterate through
    // each of the JSON objects that are returned and render them to the
    // summary table.
    $.ajax({
        url: "addresses"
    }).success(function (data, status) {
        fillAddressTable(data, status);
    });
}



//function loadAddresses() {
//
//    clearAddressTable();
//
//    var aTable = $('#contentRows');
//
//    $.ajax({
//        url: 'addresses'
//    }).success(function (data, status) {
//        $.each(data, function (index, address) {
//            aTable.append($('<tr>')
//                    .append($('<td>').append($('<a>')
//                            .attr({
//                                'data-address-id': address.addressId,
//                                'data-toggle': 'modal',
//                                'data-target': '#detailsModal'
//                            })
//                            .text(address.firstName + ' ' + address.lastName)
//                            )  //end of <a> append
//                            )   //end of <td> append
//                    .append($('<td>').text(address.city))
//                    .append($('<td>').append($('<a>')
//                            .attr({
//                                'data-address-id': address.addressId,
//                                'data-toggle': 'modal',
//                                'data-target': '#editModal'
//                            })
//                            .text('Edit')))
//                    .append($('<td>').append($('<a>').attr({'onclick': 'deleteAddress(' + address.addressId + ')'}).text('Delete')))
//                    );
//        });
//    });
//}

function clearAddressTable()
{
    $("#contentRows").empty();
}

function deleteAddress(id) {
    var answer = confirm("Do you really want to delete this Address?");

    if (answer === true) {
        $.ajax({
            type: 'DELETE',
            url: 'address/' + id
        }).success(function () {
            loadAddresses();
        });
    }
}

$('#detailsModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);

    var addressId = element.data('address-id');

    var modal = $(this);

    $.ajax({
        url: 'address/' + addressId
    }).success(function (address) {
        modal.find('#address-id').text(address.addressId);
        modal.find('#address-firstName').text(address.firstName);
        modal.find('#address-lastName').text(address.lastName);
        modal.find('#address-streetAddress').text(address.streetAddress);
        modal.find('#address-city').text(address.city);
        modal.find('#address-state').text(address.state);
        modal.find('#address-zip-code').text(address.zipCode);
    });

});

$('#editModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);

    var addressId = element.data('address-id');

    var modal = $(this);
    $.ajax({
        url: 'address/' + addressId
    }).success(function (address) {
        //use.text on text display fields like <h3> or <span>
        modal.find('#address-id').text(address.addressId);
        //use.val on input fields
        modal.find('#edit-first-name').val(address.firstName);
        modal.find('#edit-last-name').val(address.lastName);
        modal.find('#edit-street-address').val(address.streetAddress);
        modal.find('#edit-city').val(address.city);
        modal.find('#edit-state').val(address.state);
        modal.find('#edit-zip-code').val(address.zipCode);
        modal.find('#edit-address-id').val(address.addressId);
    });

});

function fillAddressTable(addressList, status) {
    // clear the previous list
    clearAddressTable();
    // grab the tbody element that will hold the new list of addresses
    var aTable = $('#contentRows');

    // render the new address data to the table
    $.each(addressList, function (index, address) {
        aTable.append($('<tr>')
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-address-id': address.addressId,
                                    'data-toggle': 'modal',
                                    'data-target': '#detailsModal'
                                })
                                .text(address.firstName + ' ' + address.lastName)
                                ) // ends the <a> tag
                        ) // ends the <td> tag for the address name
                .append($('<td>').text(address.city))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-address-id': address.addressId,
                                    'data-toggle': 'modal',
                                    'data-target': '#editModal'
                                })
                                .text('Edit')
                                ) // ends the <a> tag
                        ) // ends the <td> tag for Edit
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'onClick': 'deleteAddress(' + address.addressId + ')'
                                })
                                .text('Delete')
                                ) // ends the <a> tag
                        ) // ends the <td> tag for Delete
                ); // ends the <tr> for this Address
    }); // ends the 'each' function
}

function clearErrors() {
    $('#validationErrors').empty();
}