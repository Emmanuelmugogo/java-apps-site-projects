/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var testAddressData = [
    {
        addressId: 1,
        firstName: "Emmanuel",
        lastName: "Mugogo",
        streetAddress: "12 main st",
        city: "Columbus",
        state: "OH",
        zipCode: "41234"
    },
    {
        addressId: 2,
        firstName: "London",
        lastName: "Gilmore",
        streetAddress: "90 cast dr",
        city: "Westerville",
        state: "OH",
        zipCode: "54321"
    },
    {
        addressId: 3,
        firstName: "Julian",
        lastName: "Mourice",
        streetAddress: "53 mlk way",
        city: "southbend",
        state: "IN",
        zipCode: "45534"
    }

];

var dummyDetailsAddress =
        {
        addressId: 32,
        firstName: "Lydia",
        lastName: "Richardson",
        streetAddress: "11 shanley dr",
        city: "Austin",
        state: "TX",
        zipCode: "65234"
        };

var dummyEditAddress =
        {
        addressId: 91,
        firstName: "Elizabeth",
        lastName: "Atupele",
        streetAddress: "412 main st",
        city: "Columbus",
        state: "OH",
        zipCode: "43110"
        };
