/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.addressbookmvc.dao;

import com.tsg.addressbook.dao.AddressListDao;
import com.tsg.addressbook.dao.SearchTerm;
import com.tsg.addressbook.dto.Address;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class AddressListDaoTest {

    private AddressListDao dao;
    private Address na1;
    private Address na2;
    private Address na3;
    
    public AddressListDaoTest() {
    }
    
    @Before
    public void setUp() {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("addressListDao", AddressListDao.class);
        
        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
        cleaner.execute("delete from address");
        
        na1 = new Address();
        na1.setFirstName("Emmanuel");
        na1.setLastName("Mugogo");
        na1.setStreetAddress("12 main street");
        na1.setCity("Columbus");
        na1.setState("Ohio");
        na1.setZipCode("12345");
        
        na2 = new Address();
        na2.setFirstName("Adam");
        na2.setLastName("Coate");
        na2.setStreetAddress("205 High street");
        na2.setCity("Akron");
        na2.setState("Ohio");
        na2.setZipCode("54321");
        
        na3 = new Address();
        na3.setFirstName("Nate");
        na3.setLastName("Ward");
        na3.setStreetAddress("526 main street");
        na3.setCity("Luisville");
        na3.setState("Kentucky");
        na3.setZipCode("16165");
        
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void addGetDeleteAddress()
    {
        dao.addAddress(na1);
        Address fromDb = dao.getAddressById(na1.getAddressId());
        assertEquals(fromDb, na1);
        
        dao.removeAddress(na1.getAddressId());
        assertNull(dao.getAddressById(na1.getAddressId()));
    }
    
    @Test
    public void addUpdateAddress()
    {
        dao.addAddress(na1);
        
        na1.setCity("Detroit");
        
        dao.updateAddress(na1);
        
        Address fromDb = dao.getAddressById(na1.getAddressId());
        
        assertEquals(fromDb, na1);
    }
    
    @Test
    public void getAllAddresses()
    {
        dao.addAddress(na1);
        dao.addAddress(na2);
        
        List<Address> aList = dao.getAllAddresses();
        assertEquals(2, aList.size());
    }
    
    @Test
    public void searchAddresses()
    {
        dao.addAddress(na1);
        dao.addAddress(na2);
        dao.addAddress(na3);
        
        Map<SearchTerm,String> criteria = new HashMap<>();
        
        criteria.put(SearchTerm.LAST_NAME, "Coate");
        List<Address> aList = dao.searchAddresses(criteria);
        assertEquals(1, aList.size());
        assertEquals(na2, aList.get(0));
        
//        criteria.put(SearchTerm.LAST_NAME, "Mugogo");
//        aList = dao.searchAddresses(criteria);
//        assertEquals(1, aList.size());
//        
//        criteria.put(SearchTerm.CITY, "Columbus");
//        aList = dao.searchAddresses(criteria);
//        assertEquals(1, aList.size());
//        assertEquals(na1, aList.get(0));
//        
//        criteria.put(SearchTerm.CITY, "Akron");
//        aList = dao.searchAddresses(criteria);
//        assertEquals(0, aList.size());
////        assertEquals(na2, aList.get(0));
//        
//        criteria.put(SearchTerm.CITY, "Luisville");
//        aList = dao.searchAddresses(criteria);
//        assertEquals(0, aList.size());
    }
}
