<div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="detailsModalLabel">DVD Details</h4>
            </div>
            <div class="modal-body">
                <h3 id="dvd-id"></h3>
                <table class="table table-bordered">
                    <tr>
                        <th>DVD Title:</th>
                        <td id="dvd-dvdTitle"></td>
                    </tr>
                    <tr>
                        <th>Directors Name:</th>
                        <td id="dvd-directorsName"></td>
                    </tr>
                    <tr>
                        <th>MPAA Rating:</th>
                        <td id="dvd-mpaaRating"></td>
                    </tr>
                    <tr>
                        <th>Studio Name:</th>
                        <td id="dvd-studioName"></td>
                    </tr>
                    <tr>
                        <th>User Rating:</th>
                        <td id="dvd-userRating"></td>
                    </tr>
                    <tr>
                        <th>Release Date:</th>
                        <td id="dvd-releaseDate"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="editModalLabel">Edit DVD</h4>
            </div>
            <div class="modal-body">
                <h3 id="dvd-id"></h3>
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="edit-dvd-title" class="col-md-4 control-label">DVD Title:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="edit-dvd-title" placeholder="DVD Title" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-directors-name" class="col-md-4 control-label">Directors Name:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="edit-directors-name" placeholder="Directors Name" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-mpaa-rating" class="col-md-4 control-label">MPAA Rating:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="edit-mpaa-rating" placeholder=" G | PG | PG-13 | R | NC-17 " />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-studio-name" class="col-md-4 control-label">Studio Name:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="edit-studio-name" placeholder="Studio Name" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-user-rating" class="col-md-4 control-label">User Rating:</label>
                        <div class="col-md-8">
                            <input type="tel" class="form-control" id="edit-user-rating" placeholder=" Bad | Good | VeryGood | Excellent " />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-release-date" class="col-md-4 control-label">Release Date:</label>
                        <div class="col-md-8">
                            <input type="tel" class="form-control" id="edit-release-date" placeholder="Release Date" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8">
                            <input type="hidden" id="edit-dvd-id">
                            <button type="submit" id="edit-button" class="btn btn-default" data-dismiss="modal">Edit DVD</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>