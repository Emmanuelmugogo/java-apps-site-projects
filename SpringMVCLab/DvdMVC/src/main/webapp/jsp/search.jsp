<%-- 
    Document   : search
    Created on : Mar 28, 2016, 11:03:06 PM
    Author     : apprentice
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DVD Library</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel='shortcut icon' href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <div class="container">
            <div class="jumbotron" style="background-image: url(img/library.jpg); background-size: 100%; color: blue;">
                <!--<div class="jumbotron">--><br/>
                <!--<div class="jumbotron">--><br/>
                <!--<div class="jumbotron">--><br/>
                <!--<div class="jumbotron">--><br/>
                <!--<div class="jumbotron">--><br/>
                <h1>DVD LIBRARY</h1>
                <p>Welcome to my version of DVD Library ©Emmanuel 2016</p>
                <!--<p><a class="btn btn-primary btn-lg" href="${pageContext.request.contextPath}/home" role="button">Learn more</a></p>-->
            </div>
        </div>
        <div class="container">
            <!--<h1>DVD Library</h1>-->
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role='presentation' >
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/rest">Rest</a>
                    </li>
                    <li role='presentation' class="active">
                        <a href="${pageContext.request.contextPath}/search">Search</a>
                    </li>
                    <li role='presentation' >
                        <a href="${pageContext.request.contextPath}/stats">Stats</a>
                    </li>
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/displayDvdListNoAjax">
                            DVD List (No Ajax)
                        </a>
                    </li>
                </ul>
            </div> 
            <div class="row">
                <div class="col-md-6">
                    <h2>Search Results</h2> 
                    <%@include file="dvdSummaryTableFragment.jsp"%>  
                </div>
                <!--start of search form-->
                <div class="col-md-6">
                <h2>Search</h2>
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="search-dvd-title" class="col-md-4 control-label">DVD Title:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="search-dvd-title" placeholder="DVD Title" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="search-directors-name" class="col-md-4 control-label">Directors Name:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="search-directors-name" placeholder="Directors Name" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="search-mpaa-rating" class="col-md-4 control-label">MPAA Rating:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="search-mpaa-rating" placeholder=" G | PG | PG-13 | R | NC-17 " />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="search-studio-name" class="col-md-4 control-label">Studio Name:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="search-studio-name" placeholder="Studio Name" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="search-user-rating" class="col-md-4 control-label">User Rating:</label>
                        <div class="col-md-8">
                            <input type="tel" class="form-control" id="search-user-rating" placeholder=" Bad | Good | VeryGood | Excellent " />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="search-release-date" class="col-md-4 control-label">Release Date:</label>
                        <div class="col-md-8">
                            <input type="tel" class="form-control" id="search-release-date" placeholder="Release Date" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8">
                            <button type="submit"
                                    id="search-button"
                                    class="btn btn-default">Search</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>

        <%@include file="detailsEditModalFragment.jsp"%>

        <script src='${pageContext.request.contextPath}/js/jquery-1.11.3.min.js'></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/dvdList.js"></script>
    </body>
</html>
