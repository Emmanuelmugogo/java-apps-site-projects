<!DOCTYPE html>
<html>
    <head>
        <title>DVD Library</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css"
              rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="jumbotron" style="background-image: url(img/library.jpg); background-size: 100%; color: blue;">
                <!--<div class="jumbotron">--><br/>
                <!--<div class="jumbotron">--><br/>
                <!--<div class="jumbotron">--><br/>
                <!--<div class="jumbotron">--><br/>
                <!--<div class="jumbotron">--><br/>
                <h1>DVD LIBRARY</h1>
                <p>Welcome to my version of DVD Library �Emmanuel 2016</p>
                <!--<p><a class="btn btn-primary btn-lg" href="${pageContext.request.contextPath}/home" role="button">Learn more</a></p>-->
            </div>
        </div>

        <div class="container">
            <!--<h1>DVD Library</h1>-->
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role='presentation' >
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role='presentation' class="active">
                        <a href="${pageContext.request.contextPath}/rest">Rest</a>
                    </li>
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/search">Search</a>
                    </li>
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/stats">Stats</a>
                    </li>
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/displayDvdListNoAjax">
                            DVD List (No Ajax)
                        </a>
                    </li>
                </ul>
            </div>    
            <div class="row">
                <!-- #2: Add a col to hold the summary table - have it take up half the row -->
                <div class="col-md-6">
                    <div id="dvdTableDiv">
                        <h2>My DVDs</h2>

                        <table id="dvdTable" class="table table-hover">
                            <tr>
                                <th width="40%">DVD Title</th>
                                <th width="30%">Release Date</th>
                                <th width="15%"></th>
                                <th width="15%"></th>
                            </tr>
                            <!--
                             #3: This holds the list of dvds - we will add rows
                            dynamically
                             using jQuery
                            -->
                            <tbody id="contentRows"></tbody>
                        </table>
                    </div>
                </div> <!-- End col div -->
                <!--
                #4: Add col to hold the new dvd form - have it take up the other half of the row
                -->
                <div class="col-md-6">

                    <div id="editFormDiv" style="display: none">
                        <h2 onclick="hideEditForm()">Edit DVD</h2>

                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="add-dvd-title" class="col-md-4 control-label">
                                    DVD Title:
                                </label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-dvd-title"
                                           placeholder="DVD Title"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-directors-name" class="col-md-4 control-label">
                                    Directors Name:
                                </label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-directors-name"
                                           placeholder="Directors Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-mpaa-rating" class="col-md-4 control-label">
                                    MPAA Rating:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-mpaa-rating"
                                           placeholder="G | PG | PG-13 | R | NC-17 "/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-studio-name" class="col-md-4 control-label">Studio Name:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-studio-name"
                                           placeholder="Studio Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-user-rating" class="col-md-4 control-label">User Rating:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-user-rating"
                                           placeholder=" Bad | Good | VeryGood | Excellent "/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-release-date" class="col-md-4 control-label">Release Date:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-release-date"
                                           placeholder="Release Date"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-4">
                                    <input type="hidden" id="edit-dvd-id">
                                    <button type="button"
                                            id="edit-cancel-button"
                                            class="btn btn-default"
                                            onclick="hideEditForm()">
                                        Cancel
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button type="submit"
                                            id="edit-button"
                                            class="btn btn-default">
                                        Update DVD
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div id="addFormDiv">

                        <h2>Add New DVD</h2>

                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="add-dvd-title" class="col-md-4 control-label">
                                    DVD Title:
                                </label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-dvd-title"
                                           placeholder="DVD Title"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-directors-name" class="col-md-4 control-label">
                                    Directors Name:
                                </label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-directors-name"
                                           placeholder="Directors Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-mpaa-rating" class="col-md-4 control-label">
                                    MPAA Rating:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-mpaa-rating"
                                           placeholder=" G | PG | PG-13 | R | NC-17 "/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-studio-name" class="col-md-4 control-label">Studio Name:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-studio-name"
                                           placeholder="Studio Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-user-rating" class="col-md-4 control-label">User Rating:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-user-rating"
                                           placeholder=" Bad | Good | VeryGood | Excellent "/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-release-date" class="col-md-4 control-label">Release Date:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-release-date"
                                           placeholder="Release Date"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <button type="submit"
                                            id="add-button"
                                            class="btn btn-default">
                                        Create DVD
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> <!-- End col div -->
                <div id="validationErrors" style="color: red" />
            </div> <!-- End row div -->
        </div>
    </div>
    <!-- #5: Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/restDvdList.js"></script>
</body>
</html>
