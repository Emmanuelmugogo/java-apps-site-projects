<%-- 
    Document   : displayDvdListNoAjax
    Created on : Mar 29, 2016, 7:36:30 PM
    Author     : apprentice
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DVD Library</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel='shortcut icon' href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <div class="container">
            <div class="jumbotron" style="background-image: url(img/library.jpg); background-size: 100%; color: blue;">
            <!--<div class="jumbotron">--><br/>
            <!--<div class="jumbotron">--><br/>
            <!--<div class="jumbotron">--><br/>
            <!--<div class="jumbotron">--><br/>
            <!--<div class="jumbotron">--><br/>
                <h1>DVD LIBRARY</h1>
                <p>Welcome to my version of DVD Library ©Emmanuel 2016</p>
                <!--<p><a class="btn btn-primary btn-lg" href="${pageContext.request.contextPath}/home" role="button">Learn more</a></p>-->
            </div>
        </div>
        <div class="container">
        <!--<h1>DVD Library</h1>-->
        <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/rest">Rest</a>
                    </li>
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/search">Search</a>
                    </li>
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/stats">Stats</a>
                    </li>
                    <li role='presentation' class="active">
                        <a href="${pageContext.request.contextPath}/displayDvdListNoAjax">
                            DVD List (No Ajax)
                        </a>
                    </li>
                </ul>
            </div>          
        </div>
        
        <div class="container">
            <h1>DVD Library</h1>
            <a href="displayNewDvdFormNoAjax">Add a new DVD</a>
            <hr/>

            <c:forEach var="dvd" items="${dvdList}">
                <s:url value="deleteDvdNoAjax" var="deleteDvd_url">
                    <s:param name="dvdId" value="${dvd.dvdId}" />
                </s:url>
                <s:url value="displayEditDvdFormNoAjax" var="editDvd_url">
                    <s:param name="dvdId" value="${dvd.dvdId}" />
                </s:url>
                Title: ${dvd.dvdTitle}
                <a href="${deleteDvd_url}">|Delete</a>
                <a href="${editDvd_url}">|Edit</a><br/>
                Director's Name: ${dvd.directorsName}<br/>
                Mpaa Rating: ${dvd.mpaaRating}<br/>
                Studio Name: ${dvd.studioName}<br/>
                User Rating: ${dvd.userRating}<br/>
                Release Date: ${dvd.releaseDate}<br/>
                <hr/>
            </c:forEach>
        </div>
                            
                            
                            
                <script src='${pageContext.request.contextPath}/js/jquery-1.11.3.min.js'></script>
                <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>