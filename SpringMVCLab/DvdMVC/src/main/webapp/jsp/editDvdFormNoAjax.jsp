<%-- 
    Document   : editDvdFormNoAjax
    Created on : Mar 29, 2016, 7:37:36 PM
    Author     : apprentice
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DVD Library</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel='shortcut icon' href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <div class="container">
            <div class="jumbotron" style="background-image: url(img/library.jpg); background-size: 100%; color: blue;">
            <!--<div class="jumbotron">--><br/>
            <!--<div class="jumbotron">--><br/>
            <!--<div class="jumbotron">--><br/>
            <!--<div class="jumbotron">--><br/>
            <!--<div class="jumbotron">--><br/>
                <h1>DVD LIBRARY</h1>
                <p>Welcome to my version of DVD Library ©Emmanuel 2016</p>
                <!--<p><a class="btn btn-primary btn-lg" href="${pageContext.request.contextPath}/home" role="button">Learn more</a></p>-->
            </div>
        </div>
        <div class="container">
            <!--<h1>DVD Library</h1>-->
            <hr/>
        </div>
        <div class="container">
            <h1>Edit DVD Form</h1>
            <a href="displayDvdListNoAjax">DVD List (No Ajax)</a>
            <hr/>
            <sf:form class="form-horizontal" modelAttribute="dvd"
                  role='form'
                  action='editDvdNoAjax'
                method="POST">
                <div class="form-group">
                    <label for="add-dvd-title" class="col-md-4 control-label">DVD Title:</label>
                    <div class='col-md-8'>
                        <sf:input type="text" class="form-control" id="add-dvd-title" path="dvdTitle" placeholder="Dvd Title"/>
                        <sf:errors path="dvdTitle" cssClass="text-danger"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-directors-name" class="col-md-4 control-label">Directors Name:</label>
                    <div class='col-md-8'>
                        <sf:input type="text" class="form-control" id="add-directors-name" path="directorsName" placeholder="Directors Name"/>
                        <sf:errors path="directorsName" cssClass="text-danger"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-mpaa-rating" class="col-md-4 control-label">Mpaa Rating:</label>
                    <div class='col-md-8'>
                        <sf:input type="text" class="form-control" id="add-mpaa-rating" path="mpaaRating" placeholder="Mpaa Rating"/>
                        <sf:errors path="mpaaRating" cssClass="text-danger"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-studio-name" class="col-md-4 control-label">Studio Name:</label>
                    <div class='col-md-8'>
                        <sf:input type="text" class="form-control" id="add-studio-name" path="studioName" placeholder="Studio Name"/>
                        <sf:errors path="studioName" cssClass="text-danger"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-user-rating" class="col-md-4 control-label">User Rating:</label>
                    <div class='col-md-8'>
                        <sf:input type="text" class="form-control" id="add-user-rating" path="userRating" placeholder="User Rating"/>
                        <sf:errors path="userRating" cssClass="text-danger"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-release-date" class="col-md-4 control-label">Release Date:</label>
                    <div class='col-md-8'>
                        <sf:input type="text" class="form-control" id="add-release-date" path="releaseDate" placeholder="Release Date"/>
                        <sf:errors path="releaseDate" cssClass="text-danger"></sf:errors>
                        <sf:hidden path="dvdId"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" id="add-button" class="btn btn-default">Edit New DVD</button>
                    </div>    
                </div>
            </sf:form>
        </div>
    </body>
</html>
