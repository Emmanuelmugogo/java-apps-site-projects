<%-- 
    Document   : home
    Created on : Mar 28, 2016, 11:02:44 PM
    Author     : apprentice
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DVD Library</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel='shortcut icon' href="${pageContext.request.contextPath}/img/icon.png">
        <link rel="stylesheet" href="dvdLibrary.css">

    </head>
    <body>
        <div class="container">
            <div class="jumbotron" style="background-image: url(img/library.jpg); background-size: 100%; color: blue;">
                <!--<div class="jumbotron">--><br/>
                <!--<div class="jumbotron">--><br/>
                <!--<div class="jumbotron">--><br/>
                <!--<div class="jumbotron">--><br/>
                <!--<div class="jumbotron">--><br/>
                <h1>DVD LIBRARY</h1>
                <p>Welcome to my version of DVD Library ©Emmanuel 2016</p>
                <!--<p><a class="btn btn-primary btn-lg" href="${pageContext.request.contextPath}/home" role="button">Learn more</a></p>-->
            </div>
        </div>
        <div class="container">
            <!--<h1>DVD Library</h1>-->
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role='presentation' class="active">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/rest">Rest</a>
                    </li>
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/search">Search</a>
                    </li>
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/stats">Stats</a>
                    </li>
                    <li role='presentation'>
                        <a href="${pageContext.request.contextPath}/displayDvdListNoAjax">
                            DVD List (No Ajax)
                        </a>
                    </li>
                </ul>
            </div>          
        
        <div class="row">
            <div class="col-md-6">
                <h2>My Dvds</h2>
                <%@include file="dvdSummaryTableFragment.jsp"%> 
            </div>
            <!--start of add form-->
            <div class="col-md-6">
                <h2>Add New DVD</h2>
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="add-dvd-title" class="col-md-4 control-label">DVD Title:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="add-dvd-title" placeholder="DVD Title" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-directors-name" class="col-md-4 control-label">Directors Name:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="add-directors-name" placeholder="Directors Name" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-mpaa-rating" class="col-md-4 control-label">MPAA Rating:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="add-mpaa-rating" placeholder=" G | PG | PG-13 | R | NC-17 " />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-studio-name" class="col-md-4 control-label">Studio Name:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="add-studio-name" placeholder="Studio Name" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-user-rating" class="col-md-4 control-label">User Rating:</label>
                        <div class="col-md-8">
                            <input type="tel" class="form-control" id="add-user-rating" placeholder=" Bad | Good | VeryGood | Excellent " />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-release-date" class="col-md-4 control-label">Release Date:</label>
                        <div class="col-md-8">
                            <input type="tel" class="form-control" id="add-release-date" placeholder="Release Date" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8">
                            <button type="submit"
                                    id="add-button"
                                    class="btn btn-default">Add DVD to Library</button>
                        </div>
                    </div>
                </form>
                <div id="validationErrors" style="color: red" />
            </div>
        </div>
        </div>
            <%@include file="detailsEditModalFragment.jsp"%>
    <script src='${pageContext.request.contextPath}/js/jquery-1.11.3.min.js'></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/mockData.js"></script>
    <script src="${pageContext.request.contextPath}/js/dvdList.js"></script>
</body>
</html>
