$(document).ready(function () {
    loadDvds();

    $('#add-button').click(function (event) {

        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'dvd',
            data: JSON.stringify({
                dvdTitle: $('#add-dvd-title').val(),
                directorsName: $('#add-directors-name').val(),
                mpaaRating: $('#add-mpaa-rating').val(),
                studioName: $('#add-studio-name').val(),
                userRating: $('#add-user-rating').val(),
                releaseDate: $('#add-release-date').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            $('#add-dvd-title').val('');
            $('#add-directors-name').val('');
            $('#add-mpaa-rating').val('');
            $('#add-studio-name').val('');
            $('#add-user-rating').val('');
            $('#add-release-date').val('');
            loadDvds();
            clearErrors();
        }).error(function (data, status) {
            clearErrors();
            $.each(data.responseJSON.fieldErrors, function (index, validationError) {
                var errorDiv = $('#validationErrors');
                errorDiv.append(validationError.message).append($('<br>'));
            });
        });
    });
    
    $('#edit-button').click(function (event) {
        event.preventDefault();

        $.ajax({
            type: 'PUT',
            url: 'dvd/' + $('#edit-dvd-id').val(),
            data: JSON.stringify({
                dvdTitle: $('#edit-dvd-title').val(),
                directorsName: $('#edit-directors-name').val(),
                mpaaRating: $('#edit-mpaa-rating').val(),
                studioName: $('#edit-studio-name').val(),
                userRating: $('#edit-user-rating').val(),
                releaseDate: $('#edit-release-date').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
//            hideEditForm();
            loadDvds();
        });
    });
});


// on click for our search button
$('#search-button').click(function (event) {
    // we don’t want the button to actually submit
    // we'll handle data submission via ajax
    event.preventDefault();
    $.ajax({
        type: 'POST',
        url: 'search/dvds',
        data: JSON.stringify({
            dvdTitle: $('#search-dvd-title').val(),
            directorsName: $('#search-directors-name').val(),
            mpaaRating: $('#search-mpaa-rating').val(),
            studioName: $('#search-studio-name').val(),
            userRating: $('#search-user-rating').val(),
            releaseDate: $('#search-release-date').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function (data, status) {
        $('#search-dvd-title').val('');
        $('#search-directors-name').val('');
        $('#search-mpaa-rating').val('');
        $('#search-studio-name').val('');
        $('#search-user-rating').val('');
        $('#search-release-date').val('');
        fillDvdTable(data, status);
    });
});



function loadDvds() {
    // Make an Ajax GET call to the 'contacts' endpoint. Iterate through
    // functioneach of the JSON objects that are returned and render them to the
    // summary table.
    $.ajax({
        url: "dvds"
    }).success(function (data, status) {
        fillDvdTable(data, status);
    });
}




//function loadDvds() {
//
//    clearDvdTable();
//
//    var dTable = $('#contentRows');
//
//    $.ajax({
//        url: 'dvds'
//    }).success(function (data, status) {
//        $.each(data, function (index, dvd) {
//            dTable.append($('<tr>')
//                    .append($('<td>').append($('<a>')
//                            .attr({
//                                'data-dvd-id': dvd.dvdId,
//                                'data-toggle': 'modal',
//                                'data-target': '#detailsModal'
//                            })
//                            .text(dvd.dvdTitle)
//                            )  //end of <a> append
//                            )   //end of <td> append
//                    .append($('<td>').text(dvd.releaseDate))
//                    .append($('<td>').append($('<a>')
//                            .attr({
//                                'data-dvd-id': dvd.dvdId,
//                                'data-toggle': 'modal',
//                                'data-target': '#editModal'
//                            })
//                            .text('Edit')))
//                    .append($('<td>').append($('<a>').attr({'onclick': 'deleteDvd('+ dvd.dvdId+ ')'}).text('Delete')))
//                    );
//        });
//    });
//}

function clearDvdTable()
{
    $("#contentRows").empty();
}

function deleteDvd(id) {
    var answer = confirm("Do you really want to delete this DVD?");

    if (answer === true) {
        $.ajax({
            type: 'DELETE',
            url: 'dvd/' + id
        }).success(function () {
            loadDvds();
        });
    }
}

$('#detailsModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);

    var dvdId = element.data('dvd-id');

    var modal = $(this);

    $.ajax({
        url: 'dvd/' + dvdId
    }).success(function (dvd) {
        modal.find('#dvd-id').text(dvd.dvdId);
        modal.find('#dvd-dvdTitle').text(dvd.dvdTitle);
        modal.find('#dvd-directorsName').text(dvd.directorsName);
        modal.find('#dvd-mpaaRating').text(dvd.mpaaRating);
        modal.find('#dvd-studioName').text(dvd.studioName);
        modal.find('#dvd-userRating').text(dvd.userRating);
        modal.find('#dvd-releaseDate').text(dvd.releaseDate);
    });

});

$('#editModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);

    var dvdId = element.data('dvd-id');

    var modal = $(this);
    $.ajax({
        url: 'dvd/' + dvdId
    }).success(function (dvd) {
        //use.text on text display fields like <h3> or <span>
        modal.find('#dvd-id').text(dvd.dvdId);
        //use.val on input fields
        modal.find('#edit-dvd-title').val(dvd.dvdTitle);
        modal.find('#edit-directors-name').val(dvd.directorsName);
        modal.find('#edit-mpaa-rating').val(dvd.mpaaRating);
        modal.find('#edit-studio-name').val(dvd.studioName);
        modal.find('#edit-user-rating').val(dvd.userRating);
        modal.find('#edit-release-date').val(dvd.releaseDate);
        modal.find('#edit-dvd-id').val(dvd.dvdId);
    });

});

function fillDvdTable(dvdList, status) {
    // clear the previous list
    clearDvdTable();
    // grab the tbody element that will hold the new list of dvds
    var dTable = $('#contentRows');

    // render the new dvd data to the table
    $.each(dvdList, function (index, dvd) {
        dTable.append($('<tr>')
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-dvd-id': dvd.dvdId,
                                    'data-toggle': 'modal',
                                    'data-target': '#detailsModal'
                                })
                                .text(dvd.dvdTitle)
                                ) // ends the <a> tag
                        ) // ends the <td> tag for the dvd title
                .append($('<td>').text(dvd.releaseDate))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-dvd-id': dvd.dvdId,
                                    'data-toggle': 'modal',
                                    'data-target': '#editModal'
                                })
                                .text('Edit')
                                ) // ends the <a> tag
                        ) // ends the <td> tag for Edit
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'onClick': 'deleteDvd(' + dvd.dvdId + ')'
                                })
                                .text('Delete')
                                ) // ends the <a> tag
                        ) // ends the <td> tag for Delete
                ); // ends the <tr> for this dvd
    }); // ends the 'each' function
}

function clearErrors() {
    $('#validationErrors').empty();
}