/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var testDvdData = [
{
        dvdId: 1,
        dvdTitle: "BORAT",
        directorsName: "Mugogo",
        mpaaRating: "PG13",
        studioName: "FOX",
        userRating: "Excellent",
        releaseDate: "2007"
},
{
        dvdId: 2,
        dvdTitle: "SUPERBAD",
        directorsName: "John",
        mpaaRating: "R",
        studioName: "PARAMOUNT",
        userRating: "GOOD",
        releaseDate: "2009"
},
{
        dvdId: 3,
        dvdTitle: "COMING TO AMERICA",
        directorsName: "Landis",
        mpaaRating: "PG13",
        studioName: "PARAMOUNT",
        userRating: "Excellent",
        releaseDate: "1988"
}

];
        var dummyDetailsDvd =
{
        dvdId: 36,
        dvdTitle: "JAVA",
        directorsName: "Ilya",
        mpaaRating: "PG20",
        studioName: "SWG",
        userRating: "VERY GOOD",
        releaseDate: "2016"
};

var dummyEditDvd =
{
        dvdId: 52,
        dvdTitle: ".NET",
        directorsName: "Victor",
        mpaaRating: "PG20",
        studioName: "SWG",
        userRating: "GOOD",
        releaseDate: "2016"
};

