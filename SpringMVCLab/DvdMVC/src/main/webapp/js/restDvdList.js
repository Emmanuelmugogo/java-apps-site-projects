$(document).ready(function () {

    loadDvds();

    $('#add-button').click(function (event) {

        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/DvdMVC/dvd',
            data: JSON.stringify({
                dvdTitle: $('#add-dvd-title').val(),
                directorsName: $('#add-directors-name').val(),
                mpaaRating: $('#add-mpaa-rating').val(),
                studioName: $('#add-studio-name').val(),
                userRating: $('#add-user-rating').val(),
                releaseDate: $('#add-release-date').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            $('#add-dvd-title').val('');
            $('#add-directors-name').val('');
            $('#add-mpaa-rating').val('');
            $('#add-studio-name').val('');
            $('#add-user-rating').val('');
            $('#add-release-date').val('');
            loadDvds();
            clearErrors();
        }).error(function (data, status) {
            clearErrors();
            $.each(data.responseJSON.fieldErrors, function (index, validationError) {
                var errorDiv = $('#validationErrors');
                errorDiv.append(validationError.message).append($('<br>'));
            });
        });
    });

    $('#edit-button').click(function (event) {
        event.preventDefault();

        $.ajax({
            type: 'PUT',
            url: 'dvd/' + $('#edit-dvd-id').val(),
            data: JSON.stringify({
                dvdTitle: $('#edit-dvd-title').val(),
                directorsName: $('#edit-directors-name').val(),
                mpaaRating: $('#edit-mpaa-rating').val(),
                studioName: $('#edit-studio-name').val(),
                userRating: $('#edit-user-rating').val(),
                releaseDate: $('#edit-release-date').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            hideEditForm();
            loadDvds();
            clearErrors();
        }).error(function (data, status) {
            clearErrors();
            $.each(data.responseJSON.fieldErrors, function (index, validationError) {
                var errorDiv = $('#validationErrors');
                errorDiv.append(validationError.message).append($('<br>'));
            });
        });
    });

});

function loadDvds() {
    clearDvdTable();

    var contentRows = $('#contentRows');

    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/DvdMVC/dvds'
    }).success(function (data, status) {
        $.each(data, function (index, dvd) {
            var title = dvd.dvdTitle;
            var releaseDate = dvd.releaseDate;
            var id = dvd.dvdId;

            var row = '<tr>';
            row += '<td>' + title + '</td>';
            row += '<td>' + releaseDate + '<td>';
            row += '<td><a onclick="showEditForm(' + id + ')">Edit</a></td>';
            row += '<td><a onclick="deleteDvd(' + id + ')">Delete</a></td>';
            row += '</tr>';
            contentRows.append(row);
        });
    });
}

function clearDvdTable() {
    $('#contentRows').empty();
}

function deleteDvd(dvdId) {
    $.ajax({
        type: 'DELETE',
        url: 'dvd/' + dvdId
    }).success(function () {
        hideEditForm();
        loadDvds();
    });
}

function showEditForm(dvdId) {
    $.ajax({
        type: 'GET',
        url: 'dvd/' + dvdId
    }).success(function (dvd, status) {
        $('#edit-dvd-title').val(dvd.dvdTitle);
        $('#edit-directors-name').val(dvd.directorsName);
        $('#edit-mpaa-rating').val(dvd.mpaaRating);
        $('#edit-studio-name').val(dvd.studioName);
        $('#edit-user-rating').val(dvd.userRating);
        $('#edit-release-date').val(dvd.releaseDate);
        $('#edit-dvd-id').val(dvd.dvdId);
        $('#editFormDiv').show();
        $('#addFormDiv').hide();
    });
}

function hideEditForm() {

    $('#edit-dvd-title').val('');
    $('#edit-directors-name').val('');
    $('#edit-mpaa-rating').val('');
    $('#edit-studio-name').val('');
    $('#edit-user-rating').val('');
    $('#edit-release-date').val('');
    $('#edit-dvd-id').val('');
    $('#addFormDiv').show();
    $('#editFormDiv').hide();
}

function clearErrors() {
    $('#validationErrors').empty();
}

