/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdmvc;

import com.tsg.dvdmvc.dao.DvdListDao;
import com.tsg.dvdmvc.dao.SearchTerm;
import com.tsg.dvdmvc.dto.Dvd;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */

@Controller
public class SearchController {
    
    private final DvdListDao dao;
    
    @Inject
    public SearchController(DvdListDao dao) {
        this.dao = dao;
    }
    
    @RequestMapping(value="/search", method=RequestMethod.GET)
    public String displaySearchPage()
    {
        return "search";
    }
    
    
    @RequestMapping(value = "search/dvds", method = RequestMethod.POST)
    @ResponseBody
    public List<Dvd> searchDvds(@RequestBody Map<String, String> searchMap) {
        
        Map<SearchTerm, String> criteriaMap = new HashMap<>();

        String currentTerm = searchMap.get("dvdTitle");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.DVD_TITLE, currentTerm);
        }
        currentTerm = searchMap.get("directorsName");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.DIRECTORS_NAME, currentTerm);
        }
        currentTerm = searchMap.get("mpaaRating");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.MPAA_RATING, currentTerm);
        }
        currentTerm = searchMap.get("studioName");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.STUDIO_NAME, currentTerm);
        }
        currentTerm = searchMap.get("userRating");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.USER_RATING, currentTerm);
        }
        
        currentTerm = searchMap.get("releaseDate");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.RELEASE_DATE, currentTerm);
        }

        return dao.searchDvds(criteriaMap);
    }
    
}
