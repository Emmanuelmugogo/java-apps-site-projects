/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdmvc;

import com.tsg.dvdmvc.dao.DvdListDao;
import com.tsg.dvdmvc.dto.Dvd;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */

@Controller
public class HomeControllerNoAjax {
    private final DvdListDao dao;
    
    @Inject
    public HomeControllerNoAjax(DvdListDao dao)
    {
        this.dao = dao;
    }
    
 @RequestMapping(value="/displayDvdListNoAjax", method=RequestMethod.GET)
    public String displayDvdListNoAjax(Model model)
    {
        List<Dvd> dList = dao.getAllDvds();
        model.addAttribute("dvdList",dList);
        
        return "displayDvdListNoAjax";
    }
    
    @RequestMapping(value="displayNewDvdFormNoAjax", method=RequestMethod.GET)
    public String displayNewDvdFormNoAjax(Model model){
        Dvd dvd = new Dvd();
        model.addAttribute(dvd);
        return "newDvdFormNoAjax";
    }
    
    @RequestMapping(value="/addNewDvdNoAjax", method=RequestMethod.POST)
    public String addNewDvdNoAjax(@Valid @ModelAttribute("dvd") Dvd dvd, BindingResult result, HttpServletRequest req){
        
        
        String dvdTitle = req.getParameter("dvdTitle");
        String directorsName = req.getParameter("directorsName");
        String mpaaRating = req.getParameter("mpaaRating");
        String studioName = req.getParameter("studioName");
        String userRating = req.getParameter("userRating");
        String releaseDate = req.getParameter("releaseDate");
        
//        Dvd dvd = new Dvd();
        dvd.setdvdTitle(dvdTitle);
        dvd.setDirectorsName(directorsName);
        dvd.setMpaaRating(mpaaRating);
        dvd.setStudioName(studioName);
        dvd.setUserRating(userRating);
        dvd.setReleaseDate(releaseDate);
        
        
        if(result.hasErrors()){
            return "newDvdFormNoAjax";
        }
        dao.addDvd(dvd);
        
        return "redirect:displayDvdListNoAjax";
    }
    
    @RequestMapping(value="/deleteDvdNoAjax", method=RequestMethod.GET)
    public String deleteDvdNoAjax(HttpServletRequest req){
        
        int dvdId = Integer.parseInt(req.getParameter("dvdId"));
        
        dao.removeDvd(dvdId);
        
        return "redirect:displayDvdListNoAjax";
    }
    
    @RequestMapping(value="/displayEditDvdFormNoAjax", method=RequestMethod.GET)
    public String displayEditDvdFormNoAjax(HttpServletRequest req, Model model){
        int dvdId = Integer.parseInt(req.getParameter("dvdId"));
        
        Dvd dvd = dao.getDvdById(dvdId);
        
        model.addAttribute("dvd", dvd);
        
        return "editDvdFormNoAjax";
    }
    
    @RequestMapping(value="/editDvdNoAjax", method=RequestMethod.POST)
    public String editDvdNoAjax(@Valid @ModelAttribute("dvd") Dvd dvd, BindingResult result){
        
        if(result.hasErrors()){
            return "editDvdFormNoAjax";
        }
        dao.updateDvd(dvd);
        
        return "redirect:displayDvdListNoAjax";
    }
}

