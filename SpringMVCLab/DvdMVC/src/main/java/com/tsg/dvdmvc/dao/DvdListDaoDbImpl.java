/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdmvc.dao;

import com.tsg.dvdmvc.dto.Dvd;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class DvdListDaoDbImpl implements DvdListDao {
    
    private static final String SQL_INSERT_DVD = 
            "insert into dvd (dvd_title, directors_name, mpaa_rating, studio_name, user_rating, release_date) value (?,?,?,?,?,?)";
    
    private static final String SQL_DELETE_DVD = 
            "delete from dvd where dvd_id = ?";
    
    private static final String SQL_UPDATE_DVD = 
            "update dvd set dvd_title = ?, directors_name = ?, mpaa_rating = ?, studio_name = ?, user_rating = ?, release_date = ? where dvd_id = ?";
    
    private static final String SQL_SELECT_ALL_DVDS = 
            "select * from dvd";
    
    private static final String SQL_SELECT_DVD = 
            "select * from dvd where dvd_id = ?";
    
    private JdbcTemplate jdbcTemplate;
    
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Dvd addDvd(Dvd dvd) {
         jdbcTemplate.update(SQL_INSERT_DVD,
                dvd.getdvdTitle(),
                dvd.getDirectorsName(),
                dvd.getMpaaRating(),
                dvd.getStudioName(),
                dvd.getUserRating(),
                dvd.getReleaseDate());
        dvd.setdvdId(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));
        return dvd;
    }

    @Override
    public void removeDvd(int dvdId) {
        jdbcTemplate.update(SQL_DELETE_DVD, dvdId);
    }

    @Override
    public void updateDvd(Dvd dvd) {
        jdbcTemplate.update(SQL_UPDATE_DVD,
                dvd.getdvdTitle(),
                dvd.getDirectorsName(),
                dvd.getMpaaRating(),
                dvd.getStudioName(),
                dvd.getUserRating(),
                dvd.getReleaseDate(),
                dvd.getdvdId());
        
    }

    @Override
    public List<Dvd> getAllDvds() {
        return jdbcTemplate.query(SQL_SELECT_ALL_DVDS, new DvdMapper());
    }

    @Override
    public Dvd getDvdById(int dvdId) {
 try {
        return jdbcTemplate.queryForObject(SQL_SELECT_DVD, new DvdMapper(), dvdId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }    }

    @Override
    public List<Dvd> searchDvds(Map<SearchTerm, String> criteria) {
        if (criteria.size() == 0) {
            return getAllDvds();
        } else {
            StringBuilder sQuery = new StringBuilder("select * from dvd where ");
            int numParams = criteria.size();
            int paramPosition = 0;
            String[] paramVals = new String[numParams];
            Set<SearchTerm> keySet = criteria.keySet();
            Iterator<SearchTerm> iter = keySet.iterator();
            
            while(iter.hasNext()) {
                SearchTerm currentKey = iter.next();
                
                if(paramPosition > 0) {
                    sQuery.append(" and ");
                }
                
                sQuery.append(currentKey);
                sQuery.append(" = ? ");
                
                paramVals[paramPosition] = criteria.get(currentKey);
                paramPosition++;    
            }
            return jdbcTemplate.query(sQuery.toString(), new DvdMapper(), paramVals);
        }
        
    }
    
    
     private static final class DvdMapper implements RowMapper<Dvd> {

        @Override
        public Dvd mapRow(ResultSet rs, int i) throws SQLException {
            Dvd dvd = new Dvd();
           
            dvd.setdvdId(rs.getInt("dvd_id"));
            dvd.setdvdTitle(rs.getString("dvd_title"));
            dvd.setDirectorsName(rs.getString("directors_name"));
            dvd.setMpaaRating(rs.getString("mpaa_rating"));
            dvd.setStudioName(rs.getString("studio_name"));
            dvd.setUserRating(rs.getString("user_rating"));
            dvd.setReleaseDate(rs.getString("release_date"));
            
            return dvd;
        }
        
    }
}
