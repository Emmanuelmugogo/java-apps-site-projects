/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdmvc.dto;

import java.util.Objects;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author apprentice
 */
public class Dvd {
    
    private Integer dvdId;
    @NotEmpty(message="You must supply a value for DVD Title")
    @Length(max=50, message="DVD Title must be no more than 50 characters in length")
    private String dvdTitle;
    @NotEmpty(message="You must supply a value for Directors Name")
    @Length(max=50, message="Directors Name must be no more than 50 characters in length")
    private String directorsName;
    @NotEmpty(message="You must supply a value for Mpaa Rating")
    @Length(max=5, message="Mpaa Rating must be no more than 5 characters in length")
    private String mpaaRating;
    @NotEmpty(message="You must supply a value for Studio Name")
    @Length(max=50, message="Studio Name must be no more than 50 characters in length")
    private String studioName;
    @NotEmpty(message="You must supply a value for User Rating")
    @Length(max=50, message="User Rating must be no more than 50 characters in length")
    private String userRating;
    @NotEmpty(message="You must supply a value for Release Date")
    @Length(max=20, message="Release Date must be no more than 20 characters in length")
    private String releaseDate;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.dvdId);
        hash = 83 * hash + Objects.hashCode(this.dvdTitle);
        hash = 83 * hash + Objects.hashCode(this.directorsName);
        hash = 83 * hash + Objects.hashCode(this.mpaaRating);
        hash = 83 * hash + Objects.hashCode(this.studioName);
        hash = 83 * hash + Objects.hashCode(this.userRating);
        hash = 83 * hash + Objects.hashCode(this.releaseDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Dvd other = (Dvd) obj;
        if (!Objects.equals(this.dvdTitle, other.dvdTitle)) {
            return false;
        }
        if (!Objects.equals(this.directorsName, other.directorsName)) {
            return false;
        }
        if (!Objects.equals(this.mpaaRating, other.mpaaRating)) {
            return false;
        }
        if (!Objects.equals(this.studioName, other.studioName)) {
            return false;
        }
        if (!Objects.equals(this.userRating, other.userRating)) {
            return false;
        }
        if (!Objects.equals(this.releaseDate, other.releaseDate)) {
            return false;
        }
        if (!Objects.equals(this.dvdId, other.dvdId)) {
            return false;
        }
        return true;
    }

    public Integer getdvdId() {
        return dvdId;
    }

    public void setdvdId(Integer dvdId) {
        this.dvdId = dvdId;
    }

    public String getdvdTitle() {
        return dvdTitle;
    }

    public void setdvdTitle(String dvdTitle) {
        this.dvdTitle = dvdTitle;
    }

    public String getDirectorsName() {
        return directorsName;
    }

    public void setDirectorsName(String directorsName) {
        this.directorsName = directorsName;
    }

    public String getMpaaRating() {
        return mpaaRating;
    }

    public void setMpaaRating(String mpaaRating) {
        this.mpaaRating = mpaaRating;
    }

    public String getStudioName() {
        return studioName;
    }

    public void setStudioName(String studioName) {
        this.studioName = studioName;
    }

    public String getUserRating() {
        return userRating;
    }

    public void setUserRating(String userRating) {
        this.userRating = userRating;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }
    
}
