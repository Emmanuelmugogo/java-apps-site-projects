/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdmvc.dao;

import com.tsg.dvdmvc.dto.Dvd;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class DvdListDaoInMemImpl implements DvdListDao {
    
    private Map<Integer, Dvd> dvdMap = new HashMap<>();
    private static int dvdIdCounter = 0;

    @Override
    public Dvd addDvd(Dvd dvd) {
        dvd.setdvdId(dvdIdCounter);
        dvdIdCounter++;
        dvdMap.put(dvd.getdvdId(), dvd);
        return dvd;
    }

    @Override
    public void removeDvd(int dvdId) {
        dvdMap.remove(dvdId);
    }

    @Override
    public void updateDvd(Dvd dvd) {
        dvdMap.put(dvd.getdvdId(), dvd);
    }

    @Override
    public List<Dvd> getAllDvds() {
        Collection<Dvd> d = dvdMap.values();
        return new ArrayList(d);
    }

    @Override
    public Dvd getDvdById(int dvdId) {
        return dvdMap.get(dvdId);
    }

    @Override
    public List<Dvd> searchDvds(Map<SearchTerm, String> criteria) {
        String dvdTitleCriteria = criteria.get(SearchTerm.DVD_TITLE);
        String directorsNameCriteria = criteria.get(SearchTerm.DIRECTORS_NAME);
        String mpaaRatingCriteria = criteria.get(SearchTerm.MPAA_RATING);
        String studioNameCriteria = criteria.get(SearchTerm.STUDIO_NAME);
        String userRatingCriteria = criteria.get(SearchTerm.USER_RATING);
        String releaseDateCriteria = criteria.get(SearchTerm.RELEASE_DATE);
        
        Predicate<Dvd> dvdTitleMatches;
        Predicate<Dvd> directorsNameMatches;
        Predicate<Dvd> mpaaRatingMatches;
        Predicate<Dvd> studioNameMatches;
        Predicate<Dvd> userRatingMatches;
        Predicate<Dvd> releaseDateMatches;
        
        Predicate<Dvd> truePredicate = (d) -> {return true;};
        dvdTitleMatches = (dvdTitleCriteria == null || dvdTitleCriteria.isEmpty())? truePredicate: (d)-> d.getdvdTitle().equals(dvdTitleCriteria);
        directorsNameMatches = (directorsNameCriteria == null || directorsNameCriteria.isEmpty())? truePredicate: (d)-> d.getDirectorsName().equals(directorsNameCriteria);
        mpaaRatingMatches = (mpaaRatingCriteria == null || mpaaRatingCriteria.isEmpty())? truePredicate: (d)-> d.getMpaaRating().equals(mpaaRatingCriteria);
        studioNameMatches = (studioNameCriteria == null || studioNameCriteria.isEmpty())? truePredicate: (d)-> d.getStudioName().equals(studioNameCriteria);
        userRatingMatches = (userRatingCriteria == null || userRatingCriteria.isEmpty())? truePredicate: (d)-> d.getUserRating().equals(userRatingCriteria);
        releaseDateMatches = (releaseDateCriteria == null || releaseDateCriteria.isEmpty())? truePredicate: (d)-> d.getReleaseDate().equals(releaseDateCriteria);
        
        return dvdMap.values().stream()
                .filter(dvdTitleMatches 
                        .and(directorsNameMatches)
                        .and(mpaaRatingMatches)
                        .and(studioNameMatches)
                        .and(userRatingMatches)
                        .and(releaseDateMatches))
                .collect(Collectors.toList());
    }
    
}
