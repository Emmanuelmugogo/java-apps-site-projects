/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdlistmvc.dao;

import com.tsg.dvdmvc.dao.DvdListDao;
import com.tsg.dvdmvc.dao.SearchTerm;
import com.tsg.dvdmvc.dto.Dvd;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class DvdListDaoTest {

    private DvdListDao dao;
    private Dvd d1;
    private Dvd d2;
    private Dvd d3;
    
    public DvdListDaoTest() {
    }
    
    @Before
    public void setUp() {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("dvdListDao", DvdListDao.class);
        
        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
        cleaner.execute("delete from dvd");
        
        
        d1 = new Dvd();
        d1.setdvdTitle("Borat");
        d1.setDirectorsName("Larry");
        d1.setMpaaRating("R");
        d1.setStudioName("Fox");
        d1.setUserRating("Good");
        d1.setReleaseDate("2006");
        
        d2 = new Dvd();
        d2.setdvdTitle("Superbad");
        d2.setDirectorsName("Greg");
        d2.setMpaaRating("PG13");
        d2.setStudioName("Fox");
        d2.setUserRating("Excellent");
        d2.setReleaseDate("2007");
        
        d3 = new Dvd();
        d3.setdvdTitle("CommingToAmerica");
        d3.setDirectorsName("John");
        d3.setMpaaRating("R");
        d3.setStudioName("Paramount");
        d3.setUserRating("VeryGood");
        d3.setReleaseDate("1988");
        
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void addGetDeleteDvd(){
        dao.addDvd(d1);
        Dvd fromDb = dao.getDvdById(d1.getdvdId());
        assertEquals(fromDb, d1);
        
        dao.removeDvd(d1.getdvdId());
        assertNull(dao.getDvdById(d1.getdvdId()));
    }
    
    @Test
    public void addUpdateDvd(){
        dao.addDvd(d1);
        
        d1.setMpaaRating("PG15");
        
        dao.updateDvd(d1);
        
        Dvd fromDb = dao.getDvdById(d1.getdvdId());
        
        assertEquals(fromDb, d1);
    }
    
    @Test
    public void getAllDvds(){
        dao.addDvd(d2);
        dao.addDvd(d3);
        
        List<Dvd> dList = dao.getAllDvds();
        assertEquals(2, dList.size());
    }
    
    @Test
    public void searchDvds(){
        
        dao.addDvd(d1);
        dao.addDvd(d2);
        dao.addDvd(d3);
        
        Map<SearchTerm,String> criteria = new HashMap<>();
        
        criteria.put(SearchTerm.DVD_TITLE, "Borat");
        List<Dvd> dList = dao.searchDvds(criteria);
        assertEquals(dList.size(), 1);
        assertEquals(d1, dList.get(0));
        
//        criteria.put(SearchTerm.STUDIO_NAME, "Fox");
//        dList = dao.searchDvds(criteria);
//        assertEquals(dList.size(), 2);
//        
//        criteria.put(SearchTerm.MPAA_RATING, "R");
//        dList = dao.searchDvds(criteria);
//        assertEquals(dList.size(), 2);
//        
//        criteria.put(SearchTerm.STUDIO_NAME, "Paramount");
//        dList = dao.searchDvds(criteria);
//        assertEquals(dList.size(), 1);
//        assertEquals(dList.get(0), d3);
    }
}
