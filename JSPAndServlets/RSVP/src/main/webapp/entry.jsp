<%-- 
    Document   : entry
    Created on : Mar 24, 2016, 1:46:33 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    </head>
    <body>
        <div class="container-fluid">
        <h1>I'm having a party and you are invited!!!</h1>
        can you attend?<br/>
        <form classs="form-horizontal" action="RSVPServlet" method="POST">
            Yes<input  type="radio" name="myAnswer" value="Yes" checked /> No<input type="radio" name="myAnswer" value="No" /><br/>
            Reason (if not attending):<br/>
            <select name="myReason">
                <option value="Out of town">Out of town</option>
                <option value="Schedule conflict">Schedule conflict</option>
                <option value="I don't like you">I don't like you</option>
            </select>
            Notes:<br/>
            <input class="container-fluid" type="text" name="myNotes" /><br/>
            <input type="submit" value="RSVP" />
        </form>
        </div>
    </body>
</html>
