/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.simpletipcalculator;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class simpleTipCalculator {
    public static void main(String[] args) {
        
        Scanner Sc = new Scanner(System.in);
        Double tip = 0.0;
        Double amount = 0.0;
        int tipPercentage = 0;
        Double total = 0.0;
        
        System.out.println("Please enter the Bill Amount");
        amount = Sc.nextDouble();
        
        System.out.println("Please enter the Tip percentage");
        tipPercentage = Sc.nextInt();
        
        tip = (amount * tipPercentage/100);
        
        total = (amount + tip);
        
        System.out.println("Amount: $ " + amount);
        System.out.println("Tip percentage: " + tipPercentage + "%");
        System.out.println("Tip : $ " + tip);
        System.out.println("Total Bill: $ " + total);
        
        
        
    }
    
}
