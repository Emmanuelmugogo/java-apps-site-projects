/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.fileioexamples;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author ilyagotfryd
 */
public class FileIOExample {
    
    public static final String DELIMITER = "::";
    
    public static void main(String[] args) throws IOException {
     
        writeToFile();
        readAndPrintFile();
        
        List<Student> students = new ArrayList<>();
        
        Student student = new Student();
        student.setCohort("Java Feb 2016");
        student.setStudentFirstName("John");
        student.setStudentLastName("Doe");
        student.setStudentId("0042");
        
        students.add(student);
        
        student = new Student();
        student.setCohort("Java Feb 2016");
        student.setStudentFirstName("Jane");
        student.setStudentLastName("Smith");
        student.setStudentId("0078");
        
        students.add(student);
        
        student = new Student();
        student.setCohort("Java Sept 2015");
        student.setStudentFirstName("Joss");
        student.setStudentLastName("Whedon");
        student.setStudentId("1111");
        
        students.add(student);
        
        encodeStudents(students);
        
        List<Student> decodedList = decodeStudentList();
    }
    
    private static void encodeStudents(List<Student> students) throws IOException {
        
        PrintWriter out = new PrintWriter(new FileWriter("StudentRecords.txt"));
        
        for(Student student: students)
        {
            out.println(student.getStudentId()+ DELIMITER +
                    student.getStudentFirstName() + DELIMITER +
                    student.getStudentLastName() + DELIMITER +
                    student.getCohort());
        }
        
        out.flush();
        out.close();
        
    }
    
    public static void writeToFile() throws IOException
    {
        //this code results in the same output for variable out2
        // as the code below for variable out
//        String fileName = "OutFile.txt";
//        FileWriter fileWriter = new FileWriter(fileName);
//        PrintWriter out2 = new PrintWriter(fileWriter);
        
        PrintWriter out = new PrintWriter(new FileWriter("OutFile.txt"));
        
        out.println("this is a first line in my file...");
        out.println("this is a second line...");
        out.println("this is naturally the third line here.");
        
        // call flush to write data from memory buffer into a file.
        out.flush();
        
        out.println("write more lines before closing.");
        out.flush();
        out.close();
        
        //WARNING!:this line will not appear in the file and will not throw any exceptions.
        out.println("try to write more after closing...");
        out.flush();
        out.close();
        
    }
    
    public static void readAndPrintFile() throws FileNotFoundException
    {
        Scanner sc = new Scanner(new BufferedReader(new FileReader("OutFile.txt")));
        while(sc.hasNextLine())
        {
            String currentLine = sc.nextLine();
            System.out.println(currentLine);
        }
    }

    private static List<Student> decodeStudentList() throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader("StudentRecords.txt")));
        List<Student> resultList = new ArrayList<>();
        while(sc.hasNextLine())
        {
            String currentLine = sc.nextLine();
            Student student = new Student();
            String[] tokens = currentLine.split(DELIMITER);
            student.setStudentId(tokens[0]);
            student.setStudentFirstName(tokens[1]);
            student.setStudentLastName(tokens[2]);
            student.setCohort(tokens[3]);
            
            resultList.add(student);
        }
        
        return resultList;
    }

    
    
}
