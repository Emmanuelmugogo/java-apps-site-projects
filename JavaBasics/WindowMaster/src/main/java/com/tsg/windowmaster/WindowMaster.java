/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.windowmaster;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WindowMaster {

    public static void main(String[] args) {
        //this line won't work since we have declaration below
        //someSampleString = "";

        final float TRIM_PRICE = 2.25f;
        final float GLASS_PRICE = 3.50f;

        final float MAX_HEIGHT = 25.5f;
        final float MAX_WIDTH = 18.75f;
        final float MIN_DIMENTION = 1.0f;

        float height = 0;
        float width = 0;

        //String stringHeight = "";
        //String stringWidth = "";

        float areaOfWindow = 0;
        float cost = 0;
        float perimeterOfWindow = 0;

        // here is the sample declaration
        //String someSampleString = "";
        //Scanner sc = new Scanner(System.in);

        //System.out.println("Please enter window height:");
        //stringHeight = sc.nextLine();
        //--------------refactor level I---------
        //height = getHeight();
        //width = getWidth();
        
        //------------refactor level II-----------
        String donkeyLobster = "please enter window height";
        height = getUserInput(donkeyLobster);
        width = getUserInput("Please enetr window width");
        
        //System.out.println("Please enter window width:");
        //stringWidth = sc.nextLine();

        //height = Float.parseFloat(stringHeight);
        //width = Float.parseFloat(stringWidth);

        if (height > MAX_HEIGHT || height < MIN_DIMENTION) {
            System.out.println("Height needs to be between " + MIN_DIMENTION + " and " + MAX_HEIGHT);
        } else if (width > MAX_WIDTH || width < MIN_DIMENTION) {
            System.out.println("Width needs to be between " + MIN_DIMENTION + " and " + MAX_WIDTH);
        } else {

            areaOfWindow = height * width;
            perimeterOfWindow = 2 * (height + width); // can also do hieght*2 + width *2

            cost = ((GLASS_PRICE * areaOfWindow) + (TRIM_PRICE * perimeterOfWindow));

            System.out.println("Window height = " + height);
            System.out.println("Window width = " + width);
            System.out.println("Window area = " + areaOfWindow);
            System.out.println("Window perimeter = " + perimeterOfWindow);
            System.out.println("Total Cost = " + cost);
        }
    }

    public static float getHeight()
    {
        String stringHeight = "";
        float height = 0;
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Please enter window height:");
        stringHeight = sc.nextLine();
        height = Float.parseFloat(stringHeight);
        
        return height;
    }
    
    public static float getWidth()
    {
        String stringWidth = "";
        float width = 0;
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Please enter window width");
        stringWidth = sc.nextLine();
        width = Float.parseFloat(stringWidth);
        
        return width;
    }
    
    public static float getUserInput(String userPrompt)
    {
        String stringValue = "";
        float value = 0;
        Scanner sc = new Scanner(System.in);
        
        System.out.println(userPrompt);
        stringValue = sc.nextLine();
        value = Float.parseFloat(stringValue);
        
        return value;
    }
}