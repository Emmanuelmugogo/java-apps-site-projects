/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.arrayexamples;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class MinMaxAverageWithArrays {
    public static void main(String[] args) {
        
        int[] numbers = new int[10];
        int userInput = 0;
        Scanner sc = new Scanner(System.in);
        int minValue = 0;
        int maxValue = 0;
        double average = 0;
        
        for(int i=0; i<numbers.length;i++)
        {
            System.out.println("Enter a number ("+(i+i)+" of " + numbers.length + "):");
            userInput = sc.nextInt();
            numbers[i] = userInput;
        }
        
        minValue = maxValue = numbers[0];
        
        for(int number : numbers)
        {
//            if(number > maxValue)
//            {
//                maxValue = number;
//            }
            maxValue = (number > maxValue)?number:maxValue; //this is inline shorthand for the statement above
            
//            if(number < minValue)
//            {
//                minValue = number;
//            }
            minValue = (number < minValue)?number:minValue;
            
            average += number;
        }
        
        average = average / numbers.length;
        
        System.out.println("Min value of 10 random numbers: " + minValue);
        System.out.println("Max value of 10 random numbers: " + maxValue);
        System.out.println("Average value is: " + average);
    }
    
}
