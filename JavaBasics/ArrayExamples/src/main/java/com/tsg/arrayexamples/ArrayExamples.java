/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.arrayexamples;

/**
 *
 * @author apprentice
 */
public class ArrayExamples {
    public static void main(String[] args) {
        
        int[] teamScores;
        
        teamScores = new int[10];
        
        teamScores[0] = 2;
        teamScores[1] = 45;
        teamScores[2] = 4;
        teamScores[3] = 8;
        teamScores[4] = 99;
        teamScores[5] = 23;
        teamScores[6] = 67;
        teamScores[7] = 1;
        teamScores[8] = 88;
        teamScores[9] = 42;     
        
        int [] otherScores = {2,5,7,22,23,53,9,21,42,11};
        
        int currentScore = otherScores[3];
        otherScores[4] = 109;
        
        for(int i = 0; i<teamScores.length;i++)
        {
            
            System.out.println("Element # " + i + "=" + teamScores[i]);
        }
        
        String[] words = {"one","another","three","something","donkey lobster","banana","bean juice"};
        
        for(String monkey: words)
        {
            System.out.println("Next element is:" + monkey);
        }
        
        for(int score:teamScores)
        {
            score++;
        }
        
        for(int i = 0; i<teamScores.length;i++)
        {
            
            System.out.println("Element # " + i + "=" + teamScores[i]);
        }
    }
    
}
