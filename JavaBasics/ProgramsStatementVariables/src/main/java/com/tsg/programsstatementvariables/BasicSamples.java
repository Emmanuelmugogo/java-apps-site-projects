/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.programsstatementvariables;

/**
 *
 * @author apprentice
 */
public class BasicSamples {
    /*this is a main method
    we can have multi line comments here.
    */
    public static void main(String[] args)
    {
        System.out.println("Print something here"/*something else*/);
        System.out.println("Test this as well"); //this is a single line comment
        
//          System.out.println("Print something here"/*something else*/);
//          System.out.println("Test this as well"); //this is a single line comment

        boolean testBool = true;
        char testChar = 'Z';
        testChar = '\n';
        System.out.println("Sample String with \n some new lines");
        testChar = '\u1234';
        System.out.println("Unicode character here" + testChar);
        
        double testDouble = 3.14;
        testDouble = 3.14E-89;
        testDouble = 3.14d;
        float testFloat = 3.141234f;
        
        //this is declaration and assignment
        int testInt = 189;
        //compiler would not allow me to make this assignment
        //testInt = 9999999999999999999;
        
        testInt = 0XFA3D;  
        
        //this is exclusevly declaring a variable
        long sampleDeclaration;
    }
    
}
