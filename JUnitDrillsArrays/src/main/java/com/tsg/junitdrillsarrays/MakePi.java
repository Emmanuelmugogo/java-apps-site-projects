/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrillsarrays;

/**
 *
 * @author apprentice
 */
public class MakePi {
//    public int[] MakePi(int n) {
//
//}
//    
//}

    public int[] MakePi(int n) {
        int[] result = new int[n];
        String stringPi = (Double.toString(Math.PI * .1f)); //stringPi is .31415(...)
        stringPi = stringPi.substring(2); //stringPi is now 31415(...)
        
        //this works as well
//        stringPi = stringPi.replace('.', ' ');
//        stringPi = stringPi.trim();
        for (int i = 0; i < n; i++) { //take a one character substring at i and put it in result[n]
            result[i] = Integer.parseInt(stringPi.substring(i, i + 1));
        }
        return result;
    }
}
