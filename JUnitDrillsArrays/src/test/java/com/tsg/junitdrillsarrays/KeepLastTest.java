/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrillsarrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class KeepLastTest {
    
    public KeepLastTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of KeepLast method, of class KeepLast.
     */
    @Test
    public void testKeepLast1() {
  
        int[] numbers = {4, 5, 6};
        KeepLast X = new KeepLast();
        
        int[] expResult = {0, 0, 0, 0, 0, 6};
        int[] result = X.KeepLast(numbers);
        
        assertArrayEquals(expResult, result);
        
    }
    
    @Test
    public void testKeepLast2() {
  
        int[] numbers = {1, 2};
        KeepLast X = new KeepLast();
        
        int[] expResult = {0, 0, 0, 2};
        int[] result = X.KeepLast(numbers);
        
        assertArrayEquals(expResult, result);
        
    }
    
    @Test
    public void testKeepLast3() {
  
        int[] numbers = {3};
        KeepLast X = new KeepLast();
        
        int[] expResult = {0, 3};
        int[] result = X.KeepLast(numbers);
        
        assertArrayEquals(expResult, result);
        
    }
    
}
