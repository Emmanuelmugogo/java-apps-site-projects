/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrillsarrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MakePiTest {
    
    public MakePiTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class MakePi.
     */
    @Test
    public void testPi() {
        MakePi X = new MakePi();
        
        int[] expectedResult1 = {3};
        int[] expectedResult2 = {3, 1, 4};
        int[] expectedResult3 = {3, 1, 4, 1, 5};
        
        Assert.assertArrayEquals(expectedResult1, X.MakePi(1));
        Assert.assertArrayEquals(expectedResult2, X.MakePi(3));
        Assert.assertArrayEquals(expectedResult3, X.MakePi(5));
        
        
    }
    
}
