/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrillsarrays;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class HasEvenTest {
    
    public HasEvenTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of HasEven method, of class HasEven.
     */
    @Test
    public void testHasEven1() {
        
        int[] numbers = {2, 5};
        HasEven X = new HasEven();
        
        boolean expResult = true;
        boolean result = X.HasEven(numbers);
        
        //this also works just fine!!
        //assertEquals(expResult, result);
        assertTrue(result);
    }
    
     @Test
    public void testHasEven2() {
        
        int[] numbers = {4, 3};
        HasEven X = new HasEven();
        
        boolean expResult = true;
        boolean result = X.HasEven(numbers);
        
        //this also works just fine!!
        //assertEquals(expResult, result);
        assertTrue(result);
    }
    
     @Test
    public void testHasEven3() {
        
        int[] numbers = {7, 5};
        HasEven X = new HasEven();
        
        boolean expResult = false;
        boolean result = X.HasEven(numbers);
        
        //this also works just fine!!
        //assertEquals(expResult, result);
        assertFalse(result);
    }
    
}
