/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrillsarrays;

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Sum_JUTest {
    
    public Sum_JUTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void Test1(){
        Sum x = new Sum();
        int result;
        
        int results = 6;
        int[] numbers = {1,2,3};
        
        result = x.Sum(numbers);
        assertEquals(results, result);
    }
    
    @Test
    public void Test2(){
        Sum x = new Sum();
        int result;
        
        int results = 18;
        int[] numbers = {5,11,2};
        
        result = x.Sum(numbers);
        assertEquals(results, result);
    }
    
    public void Test3(){
        Sum x = new Sum();
        int result;
        
        int results = 7;
        int[] numbers = {7,0,0};
        
        result = x.Sum(numbers);
        assertEquals(results, result);
        
    }
}
