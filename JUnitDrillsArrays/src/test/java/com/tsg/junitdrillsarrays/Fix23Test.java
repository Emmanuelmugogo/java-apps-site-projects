/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrillsarrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Fix23Test {
    
    public Fix23Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Fix23 method, of class Fix23.
     */
    @Test
    public void testFix23Test1() {
        
        int[] numbers = {1, 2, 3};
        Fix23 X = new Fix23();
        
        int[] expResult = {1, 2, 0};
        int[] result = X.Fix23(numbers);
        
        assertArrayEquals(expResult, result);
        
    }
    
    @Test
    public void testFix23Test2() {
        
        int[] numbers = {2, 3, 5};
        Fix23 X = new Fix23();
        
        int[] expResult = {2, 0, 5};
        int[] result = X.Fix23(numbers);
        
        assertArrayEquals(expResult, result);
        
    }
    
    @Test
    public void testFix23Test3() {
        
        int[] numbers = {1, 2, 1};
        Fix23 X = new Fix23();
        
        int[] expResult = {1, 2, 1};
        int[] result = X.Fix23(numbers);
        
        assertArrayEquals(expResult, result);
        
    }
    
}
