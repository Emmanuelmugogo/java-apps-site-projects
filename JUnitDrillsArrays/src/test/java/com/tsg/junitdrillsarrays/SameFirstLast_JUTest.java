/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrillsarrays;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SameFirstLast_JUTest {
    
    public SameFirstLast_JUTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}




 @Test
    public void Test123()
    {
        SameFirstLast x = new SameFirstLast();
        
        boolean result = false;
        int[] n = {1,2,3};
        result = x.SameFirstLast(n);
        
        Assert.assertFalse(result);
        
    }
    
    @Test
    public void Test1231()
    {
        SameFirstLast x = new SameFirstLast();
        
        boolean result = false;
        int[] n = {1,2,3,1};
        result = x.SameFirstLast(n);
        
        Assert.assertTrue(result);
        
    }
    
    @Test
    public void Test121()
    {
        SameFirstLast x = new SameFirstLast();
        
        boolean result = false;
        int[] n = {1,2,1};
        result = x.SameFirstLast(n);
        
        Assert.assertTrue(result);
        
    }
    
}