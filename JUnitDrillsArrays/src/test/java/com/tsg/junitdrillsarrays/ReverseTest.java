/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrillsarrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ReverseTest {
    
    public ReverseTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Reverse method, of class Reverse.
     */
    @Test
    public void testReverse1() {
        
        int[] numbers = {1, 2, 3};
        Reverse X = new Reverse();
        int[] expResult = {3, 2, 1};
        int[] result = X.Reverse(numbers);
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void testReverse2() {
        
        int[] numbers = {4, 6, 8};
        Reverse X = new Reverse();
        int[] expResult = {8, 6, 4};
        int[] result = X.Reverse(numbers);
        assertArrayEquals(expResult, result);
    }
    
}
