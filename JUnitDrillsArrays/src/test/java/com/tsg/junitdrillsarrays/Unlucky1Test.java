/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrillsarrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Unlucky1Test {

    public Unlucky1Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of Unlucky1 method, of class Unlucky1.
     */
    @Test
    public void testUnlucky1Test1() {

        int[] numbers = {1, 3, 4, 5};
        Unlucky1 X = new Unlucky1();

        boolean expResult = true;
        boolean result = X.Unlucky1(numbers);

        assertEquals(expResult, result);
    }

    @Test
    public void testUnlucky1Test2() {

        int[] numbers = {2, 1, 3, 4, 5};
        Unlucky1 X = new Unlucky1();
        boolean expResult = true;

        boolean result = X.Unlucky1(numbers);

        assertTrue(result);

        //this also works just fine!!
        //assertEquals(expResult, result);
    }

    @Test
    public void testUnlucky1Test3() {

        int[] numbers = {1, 1, 1};
        Unlucky1 X = new Unlucky1();

        boolean expResult = false;
        boolean result = X.Unlucky1(numbers);

        assertFalse(result);
//          this one works just fine as well.
//        assertEquals(expResult, result);
    }

}
