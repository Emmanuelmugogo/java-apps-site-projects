/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrillsarrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Double23Test {
    
    public Double23Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod1() {
        Double23 X = new Double23();
        
        boolean result;
        boolean expectedResult;
        
        int[] numbers = {2, 2, 3};
        
        expectedResult = X.Double23(numbers);
        
      Assert.assertTrue(expectedResult);
    }
    
    public void testSomeMethod2() {
        Double23 X = new Double23();
        
        boolean result;
        boolean expectedResult;
        
        int[] numbers = {3, 4, 5, 3};
        
        expectedResult = X.Double23(numbers);
        
      Assert.assertTrue(expectedResult);
    }
    
    public void testSomeMethod3() {
        Double23 X = new Double23();
        
//        boolean result;
        boolean expectedResult = false;
        
        int[] numbers = {2, 3, 2, 2};
        
        boolean Result = X.Double23(numbers);
        
      Assert.assertEquals(expectedResult, Result);
    }
    
}
