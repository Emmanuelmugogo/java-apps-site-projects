/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrillsarrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class GetMiddleTest {
    
    public GetMiddleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of GetMiddle method, of class GetMiddle.
     */
    @Test
    public void testGetMiddle1() {
        
        int[] a = {1, 2, 3};
        int[] b = {4, 5, 6};
        
        GetMiddle X = new GetMiddle();
        int[] expResult = {2, 5};
        int[] result = X.GetMiddle(a, b);
        
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void testGetMiddle2() {
        
        int[] a = {7, 7, 7};
        int[] b = {3, 8, 0};
        
        GetMiddle X = new GetMiddle();
        int[] expResult = {7, 8};
        int[] result = X.GetMiddle(a, b);
        
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void testGetMiddle3() {
        
        int[] a = {5, 2, 9};
        int[] b = {1, 4, 5};
        
        GetMiddle X = new GetMiddle();
        int[] expResult = {2, 4};
        int[] result = X.GetMiddle(a, b);
        
        assertArrayEquals(expResult, result);
    }
    
}
