/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrillsarrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Make2Test {
    
    public Make2Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of make2 method, of class Make2.
     */
    @Test
    public void testMake2Test1() {
        
        int[] a = {4, 5};
        int[] b = {1, 2, 3};
        
        Make2 X = new Make2();
        
        int[] expResult = {4, 5};
        int[] result = X.make2(a, b);
        
        assertArrayEquals(expResult, result);
        
    }
    
    @Test
    public void testMake2Test2() {
        
        int[] a = {4};
        int[] b = {1, 2, 3};
        
        Make2 X = new Make2();
        
        int[] expResult = {4, 1};
        int[] result = X.make2(a, b);
        
        assertArrayEquals(expResult, result);
        
    }
    
    @Test
    public void testMake2Test3() {
        
        int[] a = {};
        int[] b = {1, 2};
        
        Make2 X = new Make2();
        
        int[] expResult = {1, 2};
        int[] result = X.make2(a, b);
        
        assertArrayEquals(expResult, result);
        
    }
    
    @Test
    public void testMake2Test4() {
        
        int[] a = {9, 8, 7, 6};
        int[] b = {1, 2, 3};
        
        Make2 X = new Make2();
        
        int[] expResult = {9, 8};
        int[] result = X.make2(a, b);
        
        assertArrayEquals(expResult, result);
        
    }
}
