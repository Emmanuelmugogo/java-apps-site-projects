/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrillsarrays;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CommonEnd_JUTest {
    
    public CommonEnd_JUTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void test1(){
        CommonEnd x = new CommonEnd();
        
        boolean result = false;
        int[] a = {1,2,3};
        int[] b = {7,3};
        
        result = x.CommonEnd(a, b);
        Assert.assertTrue(result);
     }
    
    public void test2(){
        CommonEnd x = new CommonEnd();
        
        boolean result = false;
        int[] a = {1,2,3};
        int[] b = {7,3,2};
        
        result = x.CommonEnd(a, b);
        Assert.assertFalse(result);
    }
    
    public void test3(){
        CommonEnd x = new CommonEnd();
        
        boolean result = false;
        int[] a = {1,2,3};
        int[] b = {1,3};
        
        result = x.CommonEnd(a, b);
        Assert.assertTrue(result);
    }
}
