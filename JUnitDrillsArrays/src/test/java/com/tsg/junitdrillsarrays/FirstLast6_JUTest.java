package com.tsg.junitdrillsarrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.tsg.junitdrillsarrays.FirstLast6;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FirstLast6_JUTest {
    
    public FirstLast6_JUTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
       @Test
    public void Test126()
    {
        FirstLast6 x = new FirstLast6();
        
        boolean result = false;
        int[] n = {1,2,6};
        result = x.FirstLast6(n);
        
        Assert.assertTrue(result);
        
    }
    
     @Test
    public void Test6123()
    {
        FirstLast6 x = new FirstLast6();
        
        boolean result = false;
        int[] n = {6,1,2,3};
        result = x.FirstLast6(n);
        
        Assert.assertTrue(result);
        
    }
    
     @Test
    public void Test136123()
    {
        FirstLast6 x = new FirstLast6();
        
        boolean result = false;
        int[] n = {13,6,1,2,3};
        result = x.FirstLast6(n);
        
        Assert.assertFalse(result);
        
    }
    
}
