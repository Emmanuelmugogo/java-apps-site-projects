/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrillsarrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class RotateLeftTest {
    
    public RotateLeftTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of RotateLeft method, of class RotateLeft.
     */
    @Test
    public void testRotateLeft1() {
        int[] numbers = {1, 2, 3};
        RotateLeft X = new RotateLeft();
        
        int[] expResult = {2, 3, 1};
        int[] result = X.RotateLeft(numbers);
        
        assertArrayEquals(expResult, result);
        
    }
    
    @Test
    public void testRotateLeft2() {
        int[] numbers = {5, 11, 9};
        RotateLeft X = new RotateLeft();
        
        int[] expResult = {11, 9, 5};
        int[] result = X.RotateLeft(numbers);
        
        assertArrayEquals(expResult, result);
        
    }
    
    @Test
    public void testRotateLeft3() {
        int[] numbers = {7, 0, 0};
        RotateLeft X = new RotateLeft();
        
        int[] expResult = {0, 0, 7};
        int[] result = X.RotateLeft(numbers);
        
        assertArrayEquals(expResult, result);
        
    }
    
}
