/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrillsarrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class HigherWinsTest {
    
    public HigherWinsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of HigherWins method, of class HigherWins.
     */
    @Test
    public void testHigherWins1() {
        
        int[] numbers = {1, 2, 3};
        HigherWins X = new HigherWins();
        
        int[] expResult = {3, 3, 3};
        int[] result = X.HigherWins(numbers);
        
        assertArrayEquals(expResult, result);
    }
    
}
