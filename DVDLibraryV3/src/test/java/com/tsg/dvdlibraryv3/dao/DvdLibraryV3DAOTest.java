/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdlibraryv3.dao;

import com.swcguild.dvdlibrary.dao.DvdLibraryDao;
import com.swcguild.dvdlibrary.dto.Dvd;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class DvdLibraryV3DAOTest {

    public DvdLibraryV3DAOTest() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        dao = ctx.getBean("DvdLibraryV3DAO", DvdLibraryDao.class);
    }
    DvdLibraryDao dao;
    Dvd d1;
    Dvd d2;
    Dvd d3;

    @Before
    public void setUp() {

        d1 = new Dvd();
        d1.setId(1);
        d1.setTitle("Comming to America");
//        d1.setReleaseDate(1988);
        d1.setMpaaRating("R");
        d1.setDirector("John Landis");
        d1.setStudio("Paramount");

        d2 = new Dvd();
        d2.setId(2);
        d2.setTitle("The Godfather");
//        d2.setReleaseDate(1988);
        d2.setMpaaRating("PG");
        d2.setDirector("Francis Ford");
        d2.setStudio("Paramount");

        d3 = new Dvd();
        d3.setId(3);
        d3.setTitle("Catch Me if You Can");
//        d3.setReleaseDate(1988);
        d3.setMpaaRating("PG");
        d3.setDirector("Steven Spielberg");
        d3.setStudio("Dreamworks Pictures");

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createDvd method, of class DvdLibraryV3DAO.
     */
    @Test
    public void testAdd() {
        dao.add(d1);
        dao.add(d2);
        dao.add(d3);

        List<Dvd> Test = dao.listAll();

        assertEquals(3, Test.size());
    }

    @Test
    public void testRemove() {
        dao.add(d1);
        dao.add(d2);
        dao.add(d3);

        int id = 3;
        dao.remove(id);
        int expectedResults = 2;
        List<Dvd> Result = dao.listAll();
        int remainingSize = Result.size();
        assertEquals(expectedResults, remainingSize);

    }

    /**
     * Test of listAll method, of class DvdLibraryV3DAO.
     */
    @Test
    public void testListAll() {
        dao.add(d1);
        dao.add(d2);
        dao.add(d3);

        int expResult = 3;
        List<Dvd> result = dao.listAll();
        int listSize = result.size();
        assertEquals(expResult, listSize);
    }

    /**
     * Test of getById method, of class DvdLibraryV3DAO.
     */
    @Test
    public void testGetById() {
        dao.add(d1);
        dao.add(d2);
        dao.add(d3);

        Dvd result = dao.getById(d3.getId());
        assertEquals(3, d3.getId());

    }

    /**
     * Test of getByTitle method, of class DvdLibraryV3DAO.
     */
    @Test
    public void testGetByTitle() {
        dao.add(d1);
        dao.add(d2);
        dao.add(d3);

        List<Dvd> result = dao.getByTitle(d1.getTitle());
        assertEquals(d1.getTitle(), result.get(0).getTitle());
    }

    /**
     * Test of getByRating method, of class DvdLibraryV3DAO.
     */
    @Test
    public void testGetByRating() {
        dao.add(d1);
        dao.add(d2);
        dao.add(d3);

        List<Dvd> result = dao.getByRating("PG");
        int resultSize = result.size();
        assertEquals(resultSize, 2);

    }

    /**
     * Test of getByStudio method, of class DvdLibraryV3DAO.
     */
    @Test
    public void testGetByStudio() {
        dao.add(d1);
        dao.add(d2);
        dao.add(d3);

        List<Dvd> result = dao.getByStudio("Paramount");
        int resultSize = result.size();
        assertEquals(resultSize, 2);
    }

}
