/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdlibraryv3.controller;

import com.swcguild.dvdlibrary.dao.DvdLibraryDao;
import com.swcguild.dvdlibrary.dto.Dvd;
import com.tsg.dvdlibraryv3.dao.DvdLibraryV3DAO;
import com.tsg.dvdlibraryv3.ui.ConsoleIO;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class DvdLibraryController {
    ConsoleIO con = new ConsoleIO();
    DvdLibraryDao dvdLibrary = new DvdLibraryV3DAO();

    public DvdLibraryController(DvdLibraryDao dao) {
        dvdLibrary = dao;
    }
    
    

    void run() {
        int userChoice = 0;
        boolean keepGoing = true;
//        dvdLibrary.loadDvd();
        do {
            printMenu();
            userChoice = con.readInt("Select option: ", 1, 6);
            switch (userChoice) {
                case 1:
                    addDvd();
                    break;
                case 2:
                    removeDvd();
                    break;
                case 3:
                    listAllDvds();
                    break;
                case 4:
                    searchDvd();
                    break;
                case 5:
                    editDvd();
                    break;
                case 6:
                    keepGoing = false;
                    break;
                default:
                    con.print("Invalid entry.");
                    break;
            }
        } while (keepGoing == true);
//        dvdLibrary.writeDvd();
        con.print("Thank you for using our program");
    }

    private void printMenu() {
        con.print("1. Add new Dvd");
        con.print("2. Remove an existing Dvd");
        con.print("3. list all Dvds");
        con.print("4. Search Dvd by title");
        con.print("5. Edit Dvd info");
        con.print("6. Exit");

    }

    private void addDvd() {
        String title = con.readString("Enter movie title: ");
        String releaseDate = con.readString("Enter release date: ");
        String mpaaRating = con.readString("Enter MPAA rating: ");
        String director = con.readString("Enter director name: ");
        String studio = con.readString("Enter studio: ");
        List<String> listOfRatings = new ArrayList<>();
        String userChoice = "";
        do {
            String userRating = con.readString("Enter user rating: ");
            listOfRatings.add(userRating);
            userChoice = con.readString("Do you want to enter another user rating? (Y/N)");
        } while (userChoice.equalsIgnoreCase("y"));
        Dvd dvd = new Dvd();
        dvd.setTitle(title);
        dvd.setReleaseDate(LocalDate.of(0, Month.MARCH, 0));
        dvd.setMpaaRating(mpaaRating);
        dvd.setDirector(director);
        dvd.setStudio(studio);
        dvd.setNote(title);

        dvdLibrary.add(dvd);
        con.print("New Dvd created.");

    }

    private void removeDvd() {
        int userInput = con.readInt("Enter the Dvd id you want removed: ");
            dvdLibrary.remove(userInput);
        
    }

    private void listAllDvds() {
        Collection<Dvd> temp = dvdLibrary.listAll();
        if (temp.isEmpty()) {
            con.print("You do not have any Dvds in your collection.");
        } else {
            for (Dvd temp2 : temp) {
                con.print("Key: " + temp2.getId());
                con.print("Title: " + temp2.getTitle());
                con.print("Release Date: " + temp2.getReleaseDate());
                con.print("MPAA rating: " + temp2.getMpaaRating());
                con.print("director: " + temp2.getDirector());
                con.print("Studio: " + temp2.getStudio());
                con.print("User rating: " + temp2.getNote());

            }
        }

    }

    private void searchDvd() {
        int tempKey;
        String searchTitle = con.readString("Enter the Title of the Dvd you are looking for: ");
        ArrayList<Dvd> temp = (ArrayList<Dvd>) dvdLibrary.getByTitle(searchTitle.toLowerCase());
        if (temp.isEmpty()) {
            con.print("Title could not be found");
        } else if (temp.size() > 1) {
            con.print("Multiple Titles are associated with " + searchTitle);
            for (int i = 0; i < temp.size(); i++) {
                Dvd temp2 = temp.get(i);
                con.print("Key: " + temp2.getId() + " Title: " + temp2.getTitle() + " Release Date: " + temp2.getReleaseDate());
            }
            tempKey = con.readInt("Enter the Dvd key that you want to view: ");
            for (int i = 0; i < temp.size(); i++) {
                Dvd temp2 = temp.get(i);
                if (temp2.getId() == tempKey) {
                    con.print("Title: " + temp2.getTitle());
                    con.print("Release date: " + temp2.getReleaseDate());
                    con.print("MPAA rating: " + temp2.getMpaaRating());
                    con.print("Director: " + temp2.getDirector());
                    con.print("Studio: " + temp2.getStudio());
                    con.print("user rating: " + temp2.getNote());
                }
            }
        } else {
            for (Dvd temp2 : temp) {
                con.print("Title: " + temp2.getTitle());
                con.print("Release date: " + temp2.getReleaseDate());
                con.print("MPAA rating: " + temp2.getMpaaRating());
                con.print("Director: " + temp2.getDirector());
                con.print("Studio: " + temp2.getStudio());
                con.print("user rating: " + temp2.getNote());
            }
        }

    }

    private void editDvd() {
        String again = "";
        int tempKey = con.readInt("enter the Id of the Dvd you wish to edit: ");
        Dvd tempDvd = dvdLibrary.getById(tempKey);
        
        do {
            String userChoice = con.readString("Which field would you like to change: 'Title' or 'release date' or ' MPAA rating' or 'director' or 'studio' or 'user rating'");
            switch (userChoice.toLowerCase()) {
                case "title":
                    tempDvd.setTitle(con.readString("Enter new Title: "));
                    break;
                case "release date":
                    tempDvd.setReleaseDate(LocalDate.parse(con.readString("Enter new Release date(YYYY,MM,DD): ")));
                    break;
                case "mpaa rating":
                    tempDvd.setMpaaRating(con.readString("Enter new MPAA rating: "));
                    break;
                case "director":
                    tempDvd.setDirector(con.readString("Enter new director: "));
                    break;
                case "studio":
                    tempDvd.setStudio(con.readString("Enter new studio: "));
                    break;
//                case "user rating":
//                    tempDvd.setUserRating(con.readString("Enter new user rating: "));
//                    break;
                default:
                    con.print("Invalid entry");
                    break;
            }
            again = con.readString("Do you want to change another field? (yes/no)");
        } while (again.toLowerCase().equals("yes"));
//        dvdLibrary.updateDvd(tempDvd);
    }
    
}
