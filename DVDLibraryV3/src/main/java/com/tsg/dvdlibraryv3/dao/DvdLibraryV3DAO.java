/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdlibraryv3.dao;

import com.swcguild.dvdlibrary.dao.DvdLibraryDao;
import com.swcguild.dvdlibrary.dto.Dvd;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class DvdLibraryV3DAO implements DvdLibraryDao {
    Map<Integer, Dvd> dvdMap = new HashMap<>();
    final String Dvd_FILE = "Dvdlibrary.txt";
    final String DELIMITER = "::";
    final String DELIMITER_ARRAYLIST = "<><>";
    private Integer key = 0;

    public void createDvd(Dvd dvd) {
        Set<Integer> keys = dvdMap.keySet();
        for (Integer k : keys) {
            if (k > this.key) {
                this.key = k;
            }
        }
        this.key++;
        dvd.setId(this.key);
        dvdMap.put(this.key, dvd);

    }

    public boolean deleteDvd(int userInput) {
        if (dvdMap.containsKey(userInput)) {
            dvdMap.remove(userInput);
            return true;
        } else {
            return false;
        }
    }

    public Collection<Dvd> getAllDvds() {
        return dvdMap.values();
    }

    public ArrayList<Dvd> getDvdByTitle(String searchTitle) {
        Collection<Dvd> temp = dvdMap.values();
        ArrayList<Dvd> temp2 = new ArrayList<>();
        for (Dvd temp3 : temp) {
            if (temp3.getTitle().toLowerCase().equals(searchTitle)) {
                temp2.add(temp3);
            }
        }
        return temp2;
    }

    public ArrayList<Dvd> getDvdByKey(int tempKey) {
        Set<Integer> keys = dvdMap.keySet();
        ArrayList<Dvd> temp = new ArrayList<>();
        for (Integer k : keys) {
            if (k == tempKey) {
                temp.add(dvdMap.get(k));
            }
        }
        return temp;
    }

    public void updateDvd(Dvd tempDvd) {
        Set<Integer> keys = dvdMap.keySet();
        for (Integer k : keys) {
            if (k == tempDvd.getId()) {
                dvdMap.put(k, tempDvd);
            }
        }
    }

    public void loadDvd() throws FileNotFoundException {
        Scanner load = new Scanner(new BufferedReader(new FileReader(Dvd_FILE)));

        String currentLine;
        String[] currentTokens;

        while (load.hasNext()) {
            currentLine = load.nextLine();
            currentTokens = currentLine.split(DELIMITER);

            Dvd dvd = new Dvd();
            dvd.setTitle(currentTokens[0]);
            dvd.setReleaseDate(LocalDate.parse(currentTokens[1]));
            dvd.setDirector(currentTokens[2]);
            dvd.setStudio(currentTokens[3]);
            dvd.setMpaaRating(currentTokens[4]);
            dvd.setId(Integer.parseInt(currentTokens[5]));
            ArrayList<String> userRating = new ArrayList<>();
            while (load.hasNext()) {
                int i = 0;
                currentLine = load.nextLine();
                currentTokens = currentLine.split(DELIMITER_ARRAYLIST);
                userRating.add(currentTokens[i]);
                i++;
            }
            dvd.setNote("userRating");

            dvdMap.put(dvd.getId(), dvd);
        }

        load.close();

    }

    public void writeDvd() throws IOException {
        PrintWriter writer = new PrintWriter(new FileWriter(Dvd_FILE));
        Set<Integer> keys = dvdMap.keySet();
        Integer[] k = new Integer[keys.size()];
        k = keys.toArray(k);

        for (int i = 0; i < k.length; i++) {
            Dvd dvd = dvdMap.get(k[i]);
            writer.println(dvd.getTitle() + DELIMITER + dvd.getReleaseDate() + DELIMITER
                    + dvd.getDirector() + DELIMITER + dvd.getStudio() + DELIMITER + dvd.getMpaaRating()
                    + DELIMITER + dvd.getId() + DELIMITER + dvd.getNote());
            
            
            writer.flush();

        }

        writer.close();

    }

//    public List<Dvd> getMoviesReleasedInPastNYears(int n) {
//        return dvdMap.values().stream()
//                .filter((Dvd d) -> Integer.parseInt("2016") - Integer.parseInt(d.getReleaseDate()) < n)
//                .collect(Collectors.toList());
//
//    }

    public List<Dvd> getMoviesByMpaaRating(String mpaa) {
        return dvdMap.values().stream()
                .filter(d -> d.getMpaaRating().equalsIgnoreCase(mpaa))
                .collect(Collectors.toList());
    }

    public List<Dvd> getMoviesByDirector(String director) {
        return dvdMap.values().stream()
                .filter(d -> d.getDirector().equalsIgnoreCase(director))
                .collect(Collectors.toList());
    }

    public List<Dvd> getMoviesByStudio(String studio) {
        return dvdMap.values().stream()
                .filter(d -> d.getStudio().equals(studio))
                .collect(Collectors.toList());
    }

//    public double getAverageAgeOfMovies() {
//        return dvdMap.values().stream()
//                .mapToInt((Dvd d) -> Integer.parseInt("2016") - Integer.parseInt(d.getMpaaReleaseDate()))
//                .average()
//                .getAsDouble();
//    }
//
//    public List<Dvd> getNewestMovie() {
//        Dvd newestDvd = dvdMap.values().stream()
//                .max(Comparator.comparing((Dvd d) -> Integer.parseInt(d.getReleaseDate())))
//                .get();
//
//        return dvdMap.values().stream()
//                .filter(d -> Integer.parseInt(d.getReleaseDate()) == (Integer.parseInt(newestDvd.getReleaseDate())))
//                .collect(Collectors.toList());
//    }
//
//    public List<Dvd> getOldestMovie() {
//        Dvd oldestDvd = dvdMap.values().stream()
//                .min(Comparator.comparing((Dvd d) -> Integer.parseInt(d.getReleaseDate())))
//                .get();
//
//        return dvdMap.values().stream()
//                .filter(d -> Integer.parseInt(d.getReleaseDate()) == (Integer.parseInt(oldestDvd.getReleaseDate())))
//                .collect(Collectors.toList());
//    }



    @Override
    public void add(Dvd dvd) {
        
    Set<Integer> keys = dvdMap.keySet();
        for (Integer k : keys) {
            if (k > this.key) {
                this.key = k;
            }
        }
        this.key++;
        dvd.setId(this.key);
        dvdMap.put(this.key, dvd);

    }

    @Override
    public void remove(int id) {
       if (dvdMap.containsKey(id)) {
            dvdMap.remove(id);
    }
}

    @Override
    public List<Dvd> listAll() {
        List<Dvd> dvdList = new ArrayList<>();
        Set<Integer> keys = dvdMap.keySet();
        for (Integer k : keys) {
            dvdList.add(dvdMap.get(k));
        }
        
        return dvdList;
    }

    @Override
    public Dvd getById(int id) {
        Set<Integer> keys = dvdMap.keySet();
        Dvd temp = new Dvd();
        for (Integer k : keys) {
            if (k == id) {
                temp = (dvdMap.get(k));
            }
        }
        return temp;
    }

    @Override
    public List<Dvd> getByTitle(String title) {
        
       List<Dvd> temp = new ArrayList<>();
       dvdMap.values()
                .stream()
                .map(d -> {
            System.out.println("");
            return d;
        }).filter(d -> d.getTitle().equalsIgnoreCase(title))
                .forEach(d -> {
            temp.add(d);
        });
        return temp;
    }

    @Override
    public List<Dvd> getByRating(String rating) {
        return dvdMap.values().stream()
                .filter(d -> d.getMpaaRating().equalsIgnoreCase(rating))
                .collect(Collectors.toList());
    }

    @Override
    public List<Dvd> getByStudio(String studio) {
       return dvdMap.values().stream()
                .filter(d -> d.getStudio().equals(studio))
                .collect(Collectors.toList());
    }
}
