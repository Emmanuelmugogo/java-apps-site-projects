/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.factorizerwebapp;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "factorizerServlet", urlPatterns = {"/factorizerServlet"})
public class factorizerServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int number;
        int perfect = 0;
        int counter = 0;
        int primeCounter = 0;
        boolean goodPerfect = false;
        ArrayList<Integer> factors = new ArrayList<>();

        number = Integer.parseInt(request.getParameter("userInput"));

        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                factors.add(i);
                counter++;
            }

        }

        request.setAttribute("factors", factors);
        request.setAttribute("FactorCounter", (counter - 1));

        for (int count = 1; count < number; count++) {
            if (number % count == 0) {
                perfect += count;
            }

        }
        if (perfect == number) {
            goodPerfect = true;
            String isPerfect = number + " is a perfect number";
            request.setAttribute("isPerfect", isPerfect);

        }

        if (number != perfect) {
            String notPerfect = number + " is not a perfect number.";
            request.setAttribute("notPerfect", notPerfect);
        }

        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                String notPrime = number + " is not a prime number.";
                request.setAttribute("notPrime", notPrime);
                break;
            } else {
                primeCounter++;
            }

        }
        if (primeCounter == (number
                - 2)) {
            String isPrime = number + " is a prime number.";
            request.setAttribute("isPrime", isPrime);

        }

        request.setAttribute("number", number);

        RequestDispatcher rd = request.getRequestDispatcher("factorizerResults.jsp");
        rd.forward(request, response);

//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet factorizerServlet</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet factorizerServlet at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("factorizerPage.jsp");
        rd.forward(request, response);
//        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
//        RequestDispatcher rd = request.getRequestDispatcher("factorizerResults.jsp");
//        rd.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
