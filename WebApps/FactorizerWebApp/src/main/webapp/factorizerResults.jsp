<%-- 
    Document   : factorizerResults
    Created on : Mar 27, 2016, 10:41:32 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Factorizer results</title>
    </head>
    <body>
        
        <div class="container">
            <h1>Factor Results</h1>

            <p>You entered ${number}. </p>
            <p>There are ${counter} factors of ${number}.</p>
            
            <p>The factors of ${number} are: <br>
                <c:forEach var="factors" items="${factors}">

                    ${factors} <br>

                </c:forEach>
            </p>
           
            <p>${notPerfect} ${isPerfect}</p>
            <p>${notPrime} ${isPrime}</p><br>
            

            <button onclick="location.href = 'factorizerServlet';">Factor Again</button>

        </div>
    </body>
</html>
