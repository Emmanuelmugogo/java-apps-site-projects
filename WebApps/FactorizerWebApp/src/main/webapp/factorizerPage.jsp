<%-- 
    Document   : factorizerPage
    Created on : Mar 27, 2016, 10:41:11 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Factorizer</title>
    </head>
    <body>
        <div class="container">
            <h1 style="text-align: center;">Welcome to Factorizer</h1>
                <br/>
                <h2 style="text-align: center">Please enter a number you would like to  factor</h2>

            <form action="factorizerServlet" method="POST">
                <input name="userInput" type="number" />
                <button name="submit" type="submit">Click Here to Factor</button>

            </form>
        </div>

    </body>
</html>
