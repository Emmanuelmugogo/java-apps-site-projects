<%-- 
    Document   : interestCalculatorResults
    Created on : Mar 27, 2016, 11:58:04 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Interest Calculator</title>
    </head>
    <body>
        <div class="container">
        <h1>Welcome to Interest Calculator</h1>
        
        <c:forEach var="x" items="${money}">
           
            <p><span>Year #${years}</span>: Starting balance ${principal}</p>

            <p>Interest Earned: ${interestEarned()}</p>
            <p>Ending Principal amount: ${money} </p>
            
            
        </c:forEach>
        
        
        </div>
    </body>
</html>
