<%-- 
    Document   : interestCalculator
    Created on : Mar 27, 2016, 11:57:33 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Interest Calculator</title>
    </head>
    <body>
       <div class="container">
            <h1>Welcome to Interest Calculator</h1>
            <form action='interestCalculatorServlet' method="POST">

                <p>Please enter initial Amount:</p>
                <input name="initial" type="number" />

                <p>Please enter the annual interest rate: </p>
                <input name="annualRate" type="number" />       

                <p>Please enter number of years:</p>
                <input name="years" type="number" />

                <button name="submit" type="submit">Submit</button>

            </form>
        </div>
    </body>
</html>
