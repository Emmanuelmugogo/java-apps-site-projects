/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.interestcalculatorwebapp;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "interestCalculatorServlet", urlPatterns = {"/interestCalculatorServlet"})
public class interestCalculatorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
//         Scanner keyboard = new Scanner(System.in);

        float interestRate = 0.0f;
        float principal = 0.0f;
        int years = 0;
        float interestEarned = 0.0f;
        float money = 0.0f;
        float compoundRate = 0.0f;

//        System.out.println("What is the annual interest: ");
//         currentBalance = Integer.parseInt(request.getParameter("initial"));
        interestRate = Integer.parseInt(request.getParameter("annualRate"));

//        System.out.println("What is the initial amount of principal: ");
        principal = Integer.parseInt(request.getParameter("initial"));

//        System.out.println("What is the number of years the money is to stay in the fund: ");
        years = Integer.parseInt(request.getParameter("years"));

//        System.out.println("What is the number times the interest is compounded(e.i.- 365-daily, 12-monthly, 4-quarterly): ");
        compoundRate = Integer.parseInt(request.getParameter("time"));

        money = principal;
        

        for (int i = 1;i < years + 1;i++) {
            
//            System.out.println("For year " + i + " I started with " + money + " dollars.");
            
            money = compoundLoop(money, interestRate, compoundRate);
            interestEarned = money - principal;
            
//            System.out.println("For year " + i + " I earned " + interestEarned +
//                    " and ended with " + money + " dollars.");

        }

//        System.out.println("I started with " + principal + " dollars, and after "
//                + years + "years, at " + interestRate + " percent interest, coumpounded "
//                + compoundRate + " times annualy, I ended up with " + money + " dollars.");

    }


    public static float compoundLoop(float money, float interestRate, float compoundRate) {
        float interestEarned = 0.0f;
        
        for (int i = 0; i < compoundRate; i++) {
            
            interestEarned = money * ((interestRate / 100) / compoundRate);
            money = (interestEarned + money);
            
        }
        return money;
    
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet interestCalculatorServlet</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet interestCalculatorServlet at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");


        
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         RequestDispatcher rd = request.getRequestDispatcher("interestCalculator.jsp");
        rd.forward(request, response);
//        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
