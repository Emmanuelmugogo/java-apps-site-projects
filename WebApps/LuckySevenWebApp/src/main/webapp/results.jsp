<%-- 
    Document   : results
    Created on : Mar 27, 2016, 6:34:31 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Results</title>
    </head>
    <body>
        <h1 style="text-align: left">Results</h1>
        
        <div id="resultTable" style="text-align: left">


            <p>Your starting bet was ${bet} </p>
            <p> You are broke after: ${roll} rolls</p>
            <p>You should have quit after ${bestRoll} rolls, when you had  $ ${bestBet}.</p>
            <br/>
            <br/>

        </div>
            
            <button onclick="location.href='LuckySevensPage.jsp';">Play Again</button>

    </body>
</html>
