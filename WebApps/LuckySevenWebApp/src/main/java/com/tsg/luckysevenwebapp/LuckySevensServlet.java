/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.luckysevenwebapp;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "LuckySevensServlet", urlPatterns = {"/LuckySevensServlet"})
public class LuckySevensServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LuckySevensServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LuckySevensServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);
//        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
//        Scanner userInput = new Scanner(System.in);
        Random randomGenerator = new Random();
        int bet, startingBet, roll, currentBet, currentRoll, bestBet, bestRoll;

        bet = 0;
        startingBet = 0;
        roll = 0;
        currentBet = 0;
        currentRoll = 0;
        bestBet = 0;
        bestRoll = 0;

//        System.out.println("Enter starting Bet:");
        startingBet = Integer.parseInt(request.getParameter("bet"));
        bet = startingBet;

        while (bet > 0) {
            if(bet > 0){
                int die1 = randomGenerator.nextInt(6) + 1;
                int die2 = randomGenerator.nextInt(6) + 1;
                int dieTotal = (die1 + die2);

                if (dieTotal == 7) {
                    roll = roll + 1;
                    currentRoll = currentRoll + 1;
                    bet = bet + 4;
                    currentBet = +4;
                } else {
                    roll = roll + 1;
                    currentRoll = currentRoll + 1;
                    bet = bet - 1;
                    currentBet = currentBet - 1;
                }
                if (bet > bestBet) {
                    bestBet = bet;
                    bestRoll = currentRoll;
                }
            }
           
        }
        
        request.setAttribute("roll", roll);
        request.setAttribute("bestRoll", bestRoll);
        request.setAttribute("bestBet", bestBet);
        
        RequestDispatcher rd = request.getRequestDispatcher("results.jsp");
        rd.forward(request, response);

//        System.out.println("Starting bet was: " + startingBet);
//                System.out.println("Your are broke after: " + roll + " roll(s).");
//                System.out.println("You should have quit after " + bestRoll + " roll(s) when had " + bestBet + ".");
    }

        
//        processRequest(request, response);
    

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
