/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.simpletipcalculatorwebapp;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "SimpleTipServlet", urlPatterns = {"/SimpleTipServlet", "/"})
public class SimpleTipServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SimpleTipServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SimpleTipServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("simpleTipCalculator.jsp");
        rd.forward(request, response);
//        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        NumberFormat nf = NumberFormat.getCurrencyInstance();

        
//         Scanner Sc = new Scanner(System.in);
        double tip = 0.0;
        double amount = 0.0;
        int tipPercentage = 0;
        double total = 0.0;
        
        try{
            amount = Double.parseDouble(request.getParameter("amount"));
        
        tip = Double.parseDouble(request.getParameter("tip"));
        
        tip = (amount * tipPercentage/100);
        
        total = (amount + tip);
        
        request.setAttribute("amount", nf.format(amount));
        request.setAttribute("tipPercentage", tipPercentage);
        request.setAttribute("tip", nf.format(tip));
        request.setAttribute("total", nf.format(total));
        
        }catch(Exception ex) {
            request.setAttribute("error", ex);
        }
        
        
        
//        processRequest(request, response);
        RequestDispatcher rd = request.getRequestDispatcher("stresults.jsp");
        rd.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
