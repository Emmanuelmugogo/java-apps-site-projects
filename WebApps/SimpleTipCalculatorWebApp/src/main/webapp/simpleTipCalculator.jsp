<%-- 
    Document   : simpleTipCalculator
    Created on : Mar 30, 2016, 2:31:49 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Simple Tip Calculator</title>
    </head>
    <body>
        <h1>Simple Tip Calculator</h1>
        <form action="SimpleTipServlet" method="POST">
            Bill:<input type="text" name="amount" size="10">
            <br/>
            Tip %:<input type="text" name="tipPercentage" size="10">
            <br/>
            <input type="submit" value="Get">
            
        </form>
    </body>
</html>
