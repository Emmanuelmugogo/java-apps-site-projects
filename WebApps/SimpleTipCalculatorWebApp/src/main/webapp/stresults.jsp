<%-- 
    Document   : stresults
    Created on : Mar 30, 2016, 2:32:20 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Simple Tip Calculator</title>
    </head>
    <body>
        <h1>Simple Tip Calculator</h1>
        <div>
            <p>Bill Amount: ${amount}</p>
            <p>Tip Percentage: ${tipPercentage}</p>
            <p>Tip: ${tip}</p>
            <p>Total: ${total}</p>
            <a href="SimpleTipServlet">
                <input type="button" value="go back"/>
            </a>
        </div>
    </body>
</html>
