/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmastery.dao;

import com.tsg.flooringmastery.dto.Order;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public interface OrderDAOinterface {
     public boolean isTestMode();

    public String getWorkingDate();

    public void addOrder(Order order);

    public void deleteOrder(Order order);

    public Order getOrder(Integer orderNumber);

    public void changeOrderDate(Order order, String date) throws IOException;

    public ArrayList<Order> getAllOrdersByDate(String date);

    public void changeWorkingDate(String date) throws IOException;

    public void writeOrderFile() throws IOException;

    public void appendToDeletedFile(Order order) throws IOException;

    public void loadOrderFile(String date) throws FileNotFoundException;

    public void loadConfigFile(String configFile) throws FileNotFoundException;

    public void writeConfigFile(String configFile) throws IOException;
}
