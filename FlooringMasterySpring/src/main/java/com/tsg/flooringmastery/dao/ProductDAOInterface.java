/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmastery.dao;

import com.tsg.flooringmastery.dto.Product;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public interface ProductDAOInterface {
     public ArrayList<Product> getProducts();

    public Product getProductByName(String name);

    public void loadProductFile() throws FileNotFoundException;

}

