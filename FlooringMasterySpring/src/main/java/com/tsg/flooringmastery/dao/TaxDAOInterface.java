/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmastery.dao;

import com.tsg.flooringmastery.dto.Tax;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public interface TaxDAOInterface {
    
    public ArrayList<Tax> getTaxes();

    public Double getTaxRateByState(String state);

    public void loadTaxFile() throws FileNotFoundException;

    
}
