"use strict";
var x = document.getElementById("foo");
x.onclick = function () {
    if (this.style.backgroundColor === "red") {
        this.style.backgroundColor = "aqua";

    } else {
        this.style.backgroundColor = "red";
    }
    };
    $(document).ready(function () {
        $("#bar").click(function () {
            if ($(this).hasClass("pretty-background")) {
                $(this).removeClass("pretty-background");
                $(this).addClass("ugly-background");
            } else {
                $(this).removeClass("ugly-background");
                $(this).addClass("pretty-background");
            }

        });
    
    
});