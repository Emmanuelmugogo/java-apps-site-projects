/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.lambdasandstreamexampless;

import java.time.LocalDate;
import java.time.Period;


/**
 *
 * @author apprentice
 */
public class Server {
    private String Name;
    private String Ip;
    private String Make;
    private String Ram;
    private String NumProcessors;
    private LocalDate PurchaseDate;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getIp() {
        return Ip;
    }

    public void setIp(String Ip) {
        this.Ip = Ip;
    }

    public String getMake() {
        return Make;
    }

    public void setMake(String Make) {
        this.Make = Make;
    }

    public String getRam() {
        return Ram;
    }

    public void setRam(String Ram) {
        this.Ram = Ram;
    }

    public String getNumProcessors() {
        return NumProcessors;
    }

    public void setNumProcessors(String NumProcessors) {
        this.NumProcessors = NumProcessors;
    }

    public LocalDate getPurchaseDate() {
        return PurchaseDate;
    }

    public void setPurchaseDate(LocalDate PurchaseDate) {
        this.PurchaseDate = PurchaseDate;
       
    }
    
    public long getServerAge(){
        Period p = PurchaseDate.until(LocalDate.now());
        return p.getYears();
    }
    
}
