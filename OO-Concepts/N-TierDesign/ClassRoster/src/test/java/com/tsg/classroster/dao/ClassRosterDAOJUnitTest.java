/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.classroster.dao;

import com.tsg.classroster.dto.Student;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ClassRosterDAOJUnitTest {
    
    public ClassRosterDAOJUnitTest() {
    }
    
    ClassRosterDAO dao;
    
    Student s1;
    Student s2;
    Student s3;
    
    @Before
    public void setUp() {
        
        dao = new ClassRosterDAO();
        
        s1 = new Student();
        s1.setCohort("Java June 2015");
        s1.setFirstName("John");
        s1.setLastName("Doe");
        
        s2 = new Student();
        s2.setCohort("Java Sept 2015");
        s2.setFirstName("Douglas");
        s2.setLastName("Adams");
        
        s3 = new Student();
        s3.setCohort(".Net Sept 2015");
        s3.setFirstName("Guy");
        s3.setLastName("Kiwasaki");
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void createGetStudentTest()
    {
        // arrange
        dao.createStudent(s1);
        // act
        Student result = dao.getStudent(s1.getStudentId());
        // assert
        Assert.assertEquals(s1, result);
    }
    
    @Test
    public void getAllStudentIds()
    {
        //arrange
        dao.createStudent(s1);
        dao.createStudent(s2);
        dao.createStudent(s3);
        
        List<Integer> ids = new ArrayList<>();
        ids.add(s1.getStudentId());
        ids.add(s2.getStudentId());
        ids.add(s3.getStudentId());
        //act
       Integer[] studentIDs = dao.getAllStudentIds();
       
       //assert
       Assert.assertEquals(3, studentIDs.length);
       for(Integer id : studentIDs)
       {
           Assert.assertTrue(ids.contains(id));
           ids.remove(id);
       }
    }
    
    @Test
    public void removeStudent()
    {
        //arrange
        dao.createStudent(s1);
        dao.createStudent(s2);
        dao.createStudent(s3);
        
        //act
        Student removeResult = dao.removeStudent(s2.getStudentId());
        
        //assert
        Student getResult = dao.getStudent(s2.getStudentId());
        
        Assert.assertNull(getResult);
        Assert.assertEquals(s2, removeResult);
        
        Integer[] remainingIDs = dao.getAllStudentIds();
        
        Assert.assertEquals(2, remainingIDs.length);
        
    }
    
    @Test
    public void saveLoadClassRosterTest()
    {
        //arrange
        dao.createStudent(s1);
        dao.createStudent(s2);
        dao.createStudent(s3);
        
        ClassRosterDAO readDAO = new ClassRosterDAO();
        //act
        try {
            dao.writerRoster();
        } catch (IOException ex) {
            Assert.fail();
            Logger.getLogger(ClassRosterDAOJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            readDAO.loadRoster();
        } catch (FileNotFoundException ex) {
            Assert.fail();
            Logger.getLogger(ClassRosterDAOJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        //assert
        Student readS1 = readDAO.getStudent(s1.getStudentId());
        Student readS2 = readDAO.getStudent(s2.getStudentId());
        Student readS3 = readDAO.getStudent(s3.getStudentId());
        Assert.assertEquals(s1, readS1);
        Assert.assertEquals(s2, readS2);
        Assert.assertEquals(s3, readS3);
        
        
    }
}
