/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.inheritancespecialization;

import com.tsg.inheritancespecializationexamples.employees.Manager;
import com.tsg.inheritancespecializationexamples.employees.Employee;

/**
 *
 * @author apprentice
 */
public class App {
    public static void main(String[] args) {
        
        Employee someGuy = new Employee("Jeff Dunham", "123-45-6789");
        
        someGuy.doWork();
        someGuy.creatObjectives();
        someGuy.collectBonus();
        
        System.out.println("============running boss as an employee===========");
        Employee boss = new Manager("Pointy heard boss", "321-54-0987");
        
        boss.doWork();
        boss.creatObjectives();
        boss.collectBonus();
        boss.setName("Pointy heard boss");
        
        ((Manager)boss).hire();                 //((Manager)boss) casting the manager into employee
        ((Manager)boss).fire();
        ((Manager)boss).givePerfomanceReview();
        
    }
    
}
