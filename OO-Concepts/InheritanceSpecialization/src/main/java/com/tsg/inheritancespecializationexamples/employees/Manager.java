/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.inheritancespecializationexamples.employees;

import com.tsg.inheritancespecializationexamples.employees.Employee;

/**
 *
 * @author apprentice
 */
public class Manager extends Employee { //extends the manager to have what employee has.
    
    private String title;
    
    public Manager()
    {
        super("Nemo", "098-76-4321");
        title = "PM";
        //this("Nemo", "098-76-4321");  this is also possible
    }
    
    public Manager(String name, String ssn)
    {
        super(name,ssn);
        title = "GM";
    }
    
    public void hire()
    {
        System.out.println("Welcome aboard!");
    }
    
    public void fire()
    {
        System.out.println("Your services are no longer required.");
    }
    
    public void givePerfomanceReview()
    {
        System.out.println(name + " said: You did well, try to do better!");
    }
    
    @Override      //click on the override is going to the overridden method
    public void collectBonus()
    {
        System.out.println("Collect your 20% of anual pay.");
    }
    
    @Override
    public void creatObjectives()
    {
        System.out.println("Make everyone work harder!!!");
        super.creatObjectives();
    }
    
}
