/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.tsg.exceptionsexamples.CheckedExceptionExample;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author apprentice
 */
public class CheckedExceptionUnitTests {
    
    public CheckedExceptionUnitTests() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void CanEchoValueLessThen42()
    {
        CheckedExceptionExample example = new CheckedExceptionExample();
        
        try{
            int result = example.SillyCheckedException(23);
            Assert.assertEquals(23, result);
        }catch(Exception e)
        {
            Assert.fail();
        }
    }
    
    @Test
    public void WillThrowExceptionValueMoreThen42()
    {
        CheckedExceptionExample example = new CheckedExceptionExample();
        
        try {
            int result = example.SillyCheckedException(53);
            Assert.fail();
        } catch (Exception ex) {
            Assert.assertEquals("You have gone beyound 42.", ex.getMessage());
            Assert.assertTrue(true);
        }
    }
}
