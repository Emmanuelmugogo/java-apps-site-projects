/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.exceptionsexamples;

/**
 *
 * @author apprentice
 */
public class CheckedExceptionExample {
    
    public int SillyCheckedException(int value) throws Exception
    {
        if(value>42)
        {
            throw new Exception("You have gone beyond 42.");
        }
        
        return value;
    }
    
}
