/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.mapexamples;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class MapExamples {
    public static void main(String[] args) {
        HashMap<String, Integer> population = new HashMap<>();
        
        //population.put("USA", 313000000);
        //this value is incorrect
        population.put("USA", 200000000);
        
        population.put("Canada", 34000000);
        
        population.put("United Kingdom", 63000000);
        
        population.put("Japan", 127000000);
        
        //let's see if we can correct it.
        population.put("USA", 313000000);
        
        System.out.println("Map size is: " + population.size());
        
        Integer japanPopulation = population.get("japan");
        
        System.out.println("The population of Japan is: " + japanPopulation);
        
        System.out.println("Yet again the population of Japan is: " + population.get("Japan"));
        
        System.out.println("============== printing a list of keys =============");
        //Collection<String> keys = population.keySet(); //putting in a collection is also valid
        Set<String> keys = population.keySet();
        
        for(String k : keys)
        {
            System.out.println(k);
        }
        
        System.out.println("-----------------Printing out all the values----------");
        
        for(String k : keys)
        {
            System.out.println("The population of " + k + " is " + population.get(k));
        }
        
        System.out.println("++++++++++++ printing out pure population list without keys ++++++++++++");
        Collection<Integer> popValues = population.values();
        
        for(Integer p: popValues)
        {
            System.out.println(p);
        }
        
    }
    
}
