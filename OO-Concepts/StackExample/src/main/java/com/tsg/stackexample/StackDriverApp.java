/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.stackexample;

import com.tsg.stackexample.generic.ArrayStackGeneric;
import com.tsg.stackexample.generic.StackInterfaceGeneric;
import java.util.Iterator;

/**
 *
 * @author apprentice
 */
public class StackDriverApp {
    
    public static void main(String[] args) {
        StackInterface stack = new ArrayStack();
        
        StackInterfaceGeneric<String> genericStack = new ArrayStackGeneric<>();
        
        String a = "a";
        String b = "b";
//        Integer c = 42;
        String c = "c";
        String d = "d";
        String f = "f";
//         StackInterface f = new ArrayStack();
        
        System.out.println("Pushing " + a);
        stack.push(a);
        genericStack.push(a);
        System.out.println("Pushing " + b);
        stack.push(b);
        genericStack.push(b);
        System.out.println("Pushing " + c);
        stack.push(c);
        genericStack.push(c);
        System.out.println("Pushing " + d);
        stack.push(d);
        genericStack.push(d);
        System.out.println("Pushing " + f);
        stack.push(f);
        genericStack.push(f);
        
        Iterator itr = stack.iterator();
        System.out.println("Printing values in stack using iterator...");
        while (itr.hasNext())
        {
            System.out.println(itr.next());
        }
        
        System.out.println("Printing stack values using enhanced for loop");
        
        for (Object o : stack) {
            System.out.println(o);
            
        }
        
        System.out.println("Printing GENERIC stack values with enhanced loop");
        
        for (String s : genericStack) {
            System.out.println("==> " + s);
            
        }
        
        System.out.println("Popping...");
        System.out.println("value: " + stack.pop());
        System.out.println("Popping...");
        System.out.println("value: " + stack.pop());
        System.out.println("Popping...");
        System.out.println("value: " + stack.pop());
        System.out.println("Popping...");
        System.out.println("value: " + stack.pop());
        System.out.println("Popping...");
        System.out.println("value: " + stack.pop());
        
    }
    
}
